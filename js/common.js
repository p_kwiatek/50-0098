$(document).on("click", ".caption_nav_prev a", function ()
{
    var date = $(this).attr('id').substr(2);
    $('#mod_calendar2 .module_calendar').load('index.php?c=get_calendar&date=' + date);

    $('#calendarNews').load('index.php?c=get_calendar&date=' + date);
});

$(document).on("click", ".caption_nav_next a", function ()
{
    var date = $(this).attr('id').substr(2);
    $('#mod_calendar2 .module_calendar').load('index.php?c=get_calendar&date=' + date);

    $('#calendarNews').load('index.php?c=get_calendar&date=' + date);
});

$(document).ready(function () {

    document.body.addEventListener('touchstart',function(){},false);
    
    var isMobileView = false;
    var isLargeView = false;
    var isMediumView = false;
    var isModuleHeightAlign = false;

    $('.footer__gotop').on('click', function(e) {
        e.preventDefault();
        $('html,body').animate({ scrollTop: 0 }, 'slow');
        $('.page').focus();
        return false; 
    });
    
    $('#btnFilePos').on('click', function() {
        $('#avatar_f').trigger('click');
    });

    if ((!$('.sidebar-menu > ul').length) && (!$('.sidebar-modules .module').length) && (!$('.advertLeft').length)) {
        $('.sidebar').hide();
        $('.aside__decoration--bottom').hide();
        $('.aside__decoration--circle').hide();
    }

    if ($('#mod_forum').length) {
        $('<svg xmlns="http://www.w3.org/2000/svg"xmlns:xlink="http://www.w3.org/1999/xlink"width="15px" height="12px"><defs><filter filterUnits="userSpaceOnUse" id="mod-forum-filter" x="0px" y="0px" width="15px" height="12px"  ><feOffset in="SourceAlpha" dx="0" dy="1" /><feGaussianBlur result="blurOut" stdDeviation="1" /><feFlood flood-color="rgb(49, 101, 20)" result="floodOut" /><feComposite operator="atop" in="floodOut" in2="blurOut" /><feComponentTransfer><feFuncA type="linear" slope="1"/></feComponentTransfer><feMerge><feMergeNode/><feMergeNode in="SourceGraphic"/></feMerge></filter></defs><g filter="url(#mod-forum-filter)"><path fill-rule="evenodd" d="M12.999,3.441 C12.999,1.541 10.313,0.001 7.000,0.001 C3.687,0.001 1.001,1.541 1.001,3.441 C1.001,5.066 2.968,6.428 5.612,6.787 C5.397,7.395 5.038,8.112 4.474,8.470 C5.560,8.471 6.630,7.623 7.364,6.874 C10.507,6.766 12.999,5.270 12.999,3.441 Z"/></g></svg>').prependTo('#mod_forum .button');
    }

    if ($('#mod_questionnaire').length) {
        $('#mod_questionnaire .button').wrap('<div class="button-wrapper"></div>');
        $('<svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="12px" height="12px"><defs><filter filterUnits="userSpaceOnUse" id="mod-questionnaire-filter" x="0px" y="0px" width="12px" height="12px"  ><feOffset in="SourceAlpha" dx="0" dy="1" /><feGaussianBlur result="blurOut" stdDeviation="0" /><feFlood flood-color="rgb(255, 255, 255)" result="floodOut" /><feComposite operator="atop" in="floodOut" in2="blurOut" /><feComponentTransfer><feFuncA type="linear" slope="0.59"/></feComponentTransfer><feMerge><feMergeNode/><feMergeNode in="SourceGraphic"/></feMerge></filter></defs><g filter="url(#mod-questionnaire-filter)"><path fill-rule="evenodd"  d="M0.040,10.972 L2.043,10.972 L2.043,4.966 L0.040,4.966 L0.040,10.972 ZM11.052,5.467 C11.052,4.916 10.602,4.466 10.051,4.466 L6.898,4.466 L7.398,2.163 C7.398,2.113 7.398,2.063 7.398,2.013 C7.398,1.813 7.298,1.612 7.198,1.462 L6.647,0.962 L3.344,4.265 C3.144,4.415 3.044,4.666 3.044,4.966 L3.044,9.971 C3.044,10.522 3.494,10.972 4.045,10.972 L8.550,10.972 C8.950,10.972 9.300,10.722 9.450,10.372 L10.952,6.818 C11.002,6.718 11.002,6.568 11.002,6.467 L11.002,5.467 L11.052,5.467 C11.052,5.517 11.052,5.467 11.052,5.467 Z"/></g></svg>').prependTo('#mod_questionnaire .button-wrapper');
    }

    if ($('#mod_login').length) {
        $('#mod_login .butForm').wrap('<div class="buttonWrapper"></div>');
        $('<svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="10px" height="12px"><defs><filter filterUnits="userSpaceOnUse" id="mod-login-filter" x="0px" y="0px" width="10px" height="12px"><feOffset in="SourceAlpha" dx="0" dy="1" /><feGaussianBlur result="blurOut" stdDeviation="1" /><feFlood flood-color="rgb(49, 101, 20)" result="floodOut" /><feComposite operator="atop" in="floodOut" in2="blurOut" /><feComponentTransfer><feFuncA type="linear" slope="1"/></feComponentTransfer><feMerge><feMergeNode/><feMergeNode in="SourceGraphic"/></feMerge></filter></defs><g filter="url(#mod-login-filter)"><path fill-rule="evenodd" d="M8.009,8.158 C8.009,8.159 8.009,8.159 8.009,8.159 L8.009,8.270 C8.009,8.660 7.714,8.974 7.350,8.974 L1.679,8.974 C1.316,8.974 1.021,8.660 1.021,8.270 L1.021,3.984 L2.008,3.984 L2.011,2.641 C2.023,1.198 3.158,0.038 4.543,0.053 C5.928,0.072 7.044,1.260 7.033,2.702 L7.029,3.984 L8.009,3.984 L8.009,4.141 L8.009,4.141 L8.009,8.158 ZM3.830,8.102 L4.520,8.102 L4.520,8.000 L5.185,8.000 L4.965,6.484 L4.934,6.288 C4.935,6.288 4.935,6.287 4.936,6.286 L4.934,6.275 C5.116,6.137 5.232,5.915 5.240,5.661 C5.223,5.273 4.910,4.961 4.520,4.959 L4.520,4.858 C4.518,4.858 4.517,4.857 4.515,4.857 C4.112,4.857 3.784,5.205 3.784,5.637 C3.784,5.909 3.916,6.148 4.114,6.287 L3.830,8.102 ZM4.536,0.931 C3.543,0.921 3.018,1.692 3.012,2.650 L3.008,3.984 L6.027,3.984 L6.031,2.695 C6.037,1.734 5.529,0.945 4.536,0.931 Z"/></g></svg>').prependTo('#mod_login .button, #mod_login .buttonWrapper');
    }

    if (!($('#navbar-top > ul').length)) {
        $('.top__mask').hide();
    }

    $('.menu-top .dropdown-submenu a.selected').closest('.dropdown-submenu').children('a').addClass('selected');

    if (isMobile.any) {
        $('body').addClass('is-mobile');

        $('.dropdown-submenu', '#navbar-top').on('hide.bs.dropdown', function () {
            return false;
        });
    }

    $(".board table, .main-text table").each(function() {
        var $table = $(this);
        
        if ($table.parent('.table-responsive').length === 0) {
            $table.wrap('<div class="table-responsive"></div>');
        }
    });

    $('.protectedPage').each(function(index) {
        $('<svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" viewBox="0 0 7 10" class="lock-object"><defs><filter filterUnits="userSpaceOnUse" id="lock-filter-' + index + '" x="0px" y="0px" width="7px" height="10px"><feOffset in="SourceAlpha" dx="0" dy="1" /><feGaussianBlur result="blurOut" stdDeviation="0" /><feFlood flood-color="rgb(255, 255, 255)" result="floodOut" /><feComposite operator="atop" in="floodOut" in2="blurOut" /><feComponentTransfer><feFuncA type="linear" slope="0.59"/></feComponentTransfer><feMerge><feMergeNode/><feMergeNode in="SourceGraphic"/></feMerge></filter></defs><g filter="url(#lock-filter-' + index + ')"><path fill-rule="evenodd" d="M7.009,8.158 C7.009,8.159 7.009,8.159 7.009,8.159 L7.009,8.270 C7.009,8.660 6.714,8.974 6.350,8.974 L0.679,8.974 C0.316,8.974 0.021,8.660 0.021,8.270 L0.021,3.984 L1.008,3.984 L1.011,2.641 C1.023,1.198 2.158,0.038 3.543,0.053 C4.928,0.072 6.044,1.260 6.033,2.702 L6.029,3.984 L7.009,3.984 L7.009,4.141 L7.009,4.141 L7.009,8.158 ZM2.830,8.102 L3.520,8.102 L3.520,8.000 L4.185,8.000 L3.965,6.484 L3.934,6.288 C3.935,6.288 3.935,6.287 3.936,6.286 L3.934,6.275 C4.116,6.137 4.232,5.915 4.240,5.661 C4.223,5.273 3.910,4.961 3.520,4.959 L3.520,4.858 C3.518,4.858 3.517,4.857 3.515,4.857 C3.112,4.857 2.784,5.205 2.784,5.637 C2.784,5.909 2.916,6.148 3.114,6.287 L2.830,8.102 ZM3.536,0.931 C2.543,0.921 2.018,1.692 2.012,2.650 L2.008,3.984 L5.027,3.984 L5.031,2.695 C5.037,1.734 4.529,0.945 3.536,0.931 Z"/></g></svg>').appendTo(this);    
    });

    $(".owl-carousel").owlCarousel({
        singleItem: true,
        autoPlay: 1e3 * settings.duration,
        slideSpeed: 1e3 * settings.animationDuration,
        paginationSpeed: 1e3 * settings.animationDuration,
        transitionStyle: settings.transition,
        pagination: false,
        mouseDrag: false,
        touchDrag: false
    });

    function equalizeHeights(selector) {
        var heights = [];
        $(selector).each(function() {
            $(this).css('min-height', '0');
            $(this).css('max-height', 'none');
            $(this).css('height', 'auto');
            heights.push($(this).outerHeight());
        });

        var max = Math.max.apply( Math, heights );

        $(selector).each(function() {
            $(this).css('height', max + 'px');
        }); 
    }

    function fancyboxInit() {
        var a;
        $("a.fancybox").fancybox({
            overlayOpacity: .9,
            overlayColor: settings.overlayColor,
            titlePosition: "outside",
            titleFromAlt: !0,
            titleFormat: function (a, b, c, d) {
                return '<span id="fancybox-title-over">' + texts.image + " " + (c + 1) + " / " + b.length + "</span>" + (a.length ? " &nbsp; " + a.replace(texts.enlargeImage + ": ", "") : "")
            },
            onStart: function (b, c, d) {
                a = b[c];
            },
            onComplete: function () {
            },
            onClosed: function () {
            }
        });
    }

    fancyboxInit();

    function watch() {
        var s = Snap(document.getElementById("clock"));
        var seconds = s.select("#handSecond"),
            minutes = s.select("#handMinute"),
            hours = s.select("#handHour"),
            face = {
                elem: s.select("#face"),
                cx: s.select("#face").getBBox().cx,
                cy: s.select("#face").getBBox().cy
            },
            angle = 0;
        function update() {
            var time = new Date();
            setHours(time);
            setMinutes(time);
            setSeconds(time);
        }
        function setHours(t) {
            var hour = t.getHours();
            hour %= 12;
            hour += Math.floor(t.getMinutes()/10)/6;
            var angle = hour*360/12;
            hours.animate(
                {transform: "rotate("+angle+" "+face.cx+" "+face.cy+")"},
                100,
                mina.linear,
                function(){
                    if (angle === 360) {
                        hours.attr({transform: "rotate("+0+" "+face.cx+" "+face.cy+")"});               
                    }
                }
              );
        }
        function setMinutes(t) {
            var minute = t.getMinutes();
            minute %= 60;
            minute += Math.floor(t.getSeconds()/10)/6;
            var angle = minute*360/60;
            minutes.animate(
                {transform: "rotate("+angle+" "+face.cx+" "+face.cy+")"},
                100,
                mina.linear,
                function() {
                    if (angle === 360) {
                        minutes.attr({transform: "rotate("+0+" "+face.cx+" "+face.cy+")"});
                    }
                }
            );
        }
        function setSeconds(t) {
            t = t.getSeconds();
            t %= 60;
            var angle = t*360/60;
            if (angle === 0) angle = 360;
            seconds.animate(
                {transform: "rotate("+angle+" "+face.cx+" "+face.cy+")"},
                600,
                mina.elastic,
                function(){
                    if (angle === 360) {
                        seconds.attr({transform: "rotate("+0+" "+face.cx+" "+face.cy+")"});     
                    }
                }
            );
        }
        update();
        setInterval(update, 1000);
    }

    if ($('#face').length > 0) {
        watch();
    }

    if (popup.show)
    {
        $.fancybox(
                popup.content,
                {
                    overlayOpacity: .9,
                    overlayColor: settings.overlayColor,
                    padding: 20,
                    autoDimensions: !1,
                    width: popup.width,
                    height: popup.height,
                    transitionIn: "fade",
                    transitionOut: "fade",
                    onStart: function (a) {
                        $("#fancybox-outer").css({
                            background: popup.popupBackground
                        });
                        $("#fancybox-content").addClass("main-text no-margin");
                    }
                }
        );
    }

    $('.toolbar__search').on('click', 'button', function (e) {
        if (Modernizr.mq('(max-width: 639px)')) {
            e.stopPropagation();

            if (!$(e.delegateTarget).hasClass('shown')) {
                e.preventDefault();
                $(e.delegateTarget).addClass('shown');
                $(e.delegateTarget).find('input[type=text]').focus();
                $('body').one('click', function (e) {
                    $('.toolbar__search').removeClass('shown');
                });
            }
        }
    });

    $('.toolbar__links .search').on('click', 'input', function (e) {
        e.stopPropagation();
    });
    
    var current_breakpoint = getCurrentBreakpoint();

    $(window).on('load', function() {
        updateUI();
        $('svg').wrap('<div aria-hidden="true"></div>');
    });

    $(window).on('resize', function() {

        var _cb = getCurrentBreakpoint();
        
        if (current_breakpoint !== _cb) {
            current_breakpoint = _cb;
            updateUI();
        }
        
    });

    function updateUI() {
        if (Modernizr.mq('(max-width: 991px)') && !isMobileView) {
            isMobileView = true;
            isMediumView = false;
            isLargeView = false;
            $('.header-address', '#banner').prependTo('.header-name', '#banner');
            $('.header-image-1').prependTo('.header-address', '#banner');
            $('.header-image-2').prependTo('.header-welcome .message');
            $('#searchbar').insertAfter('#fonts');
            $('#content').prependTo('#content-mobile');
            $('.header__title, .header__address').prependTo('.header__holder--mobile');
            $('#sidebar-menu').prependTo('#sidebar-menu-mobile');
            $('.modules-top').prependTo('#modules-top-mobile');
            $('#modules-top2').prependTo('#modules-top2-mobile');
            $('#modules-bottom').prependTo('#modules-bottom-mobile');
            equalizeHeights('#modules-top2 .module');
            equalizeHeights('#modules-bottom .module');
            equalizeHeights('.sidebar-modules .module');
        } 
        else if (Modernizr.mq('(min-width: 992px)') && isMobileView) {
            isMobileView = false;
            $('.header-address', '#banner').insertAfter('.header-name', '#banner');
            $('.header-image-1').insertAfter('.header-address', '#banner');
            $('.header-image-2').insertAfter('.header-address', '#banner');
            $('#searchbar').prependTo('#searchbar-desktop');
            $('#content').prependTo('#content-desktop');
            $('.header__title, .header__address').prependTo('.header__info');
            $('#sidebar-menu').prependTo('#sidebar-menu-desktop');
            $('.modules-top').prependTo('#modules-top-desktop');
            $('#modules-top2').prependTo('#modules-top2-desktop');
            $('#modules-bottom').prependTo('#modules-bottom-desktop');
        }
        if (Modernizr.mq('(min-width: 1200px)') && !isLargeView) {
            isLargeView = true;
            isMediumView = false;
            isMobileView = false;
            equalizeHeights('#modules-top2 .module');
            equalizeHeights('#modules-bottom .module');
            // $('.top__mask').width($('.top__mask').height());
        }
        else if (Modernizr.mq('(min-width: 992px)') && Modernizr.mq('(max-width: 1199px)') && !isMediumView) {
            isMediumView = true;
            isLargeView = false;
            isMobileView = false;
            equalizeHeights('#modules-top2 .module');
            equalizeHeights('#modules-bottom .module');
            // $('.top__mask').width($('.top__mask').height());
        }
    }

    $('#navbar-top').on('click', '.dropdown-submenu > a', function (e) {

        var $this = $(this);
        var $submenu = $this.next('.dropdown-menu');

        if ($submenu.length > 0) {            
            if (isMobile.any) {

                if ($this.parent().hasClass('open')) {
                    window.location.href = $this.attr('href');
                }

                $this.parent().addClass('open');

            } 
            else {
                if ($this.parent().hasClass('open')) {
                    window.location.href = $this.attr('href');
                }
            }
        }

    });

    $('#sidebar-menu').on('click', 'a', function (e) {
        $(window).trigger('resize');

        var $this = $(this);
        var $sublist = $this.next('.dropdown-menu');

        if ($sublist.is(':hidden')) {
            e.preventDefault();
            $sublist.show();
        }
        else if (!$sublist.is(':hidden')) {
            window.location.href = $this.attr('href');
        }

    });

    $('.menus a.selected', '#sidebar-menu').parents('.dropdown-menu').show();
    
});

function getCurrentBreakpoint() {
    
    var breakpoints = [0, 768, 992, 1200].reverse();
    
    for (var i=0; i < breakpoints.length; i++) {
        if (Modernizr.mq('(min-width: ' + breakpoints[i] + 'px)')) {
            return i;
        }
    }
}
