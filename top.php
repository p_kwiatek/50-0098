<div id="popup"></div>
<div class="container">
    <div class="row">
        <div class="col-xs-12">
            <div class="top">
                <div class="top__mask"><div></div></div>
                <header class="header">
                    <div class="header__holder">
                        <svg class="header__object" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="107" height="105" viewBox="0 0 107 105">
                            <path d="M102.5,64.2c-0.5,0.2-1,0.1-1.4-0.2L25.5,3.9l-6.3,94.5c-0.1,0.7-0.7,1.3-1.4,1.2c-0.7,0-1.3-0.7-1.2-1.4l6.5-97
                                c0.1-1,1.4-1.5,2.2-0.9L102.8,62c0.6,0.5,0.6,1.3,0.2,1.8C102.9,64,102.7,64.1,102.5,64.2"/>
                            <g>
                                <path d="M20.9,98.5l-2.5,1c-0.7,0.3-1.5,0-1.7-0.7c-0.3-0.7,0-1.4,0.7-1.7l2.5-1c0.7-0.3,1.5,0,1.7,0.7
                                    C21.9,97.4,21.6,98.2,20.9,98.5"/>
                                <path d="M94.8,67.5l-5.3,2.2c-0.7,0.3-1.5,0-1.8-0.7c-0.3-0.7,0-1.4,0.7-1.7l5.3-2.2c0.7-0.3,1.4,0,1.7,0.7
                                    C95.7,66.4,95.4,67.2,94.8,67.5"/>
                                <path d="M84.2,71.9l-5.3,2.2c-0.7,0.3-1.5,0-1.7-0.7c-0.3-0.6,0-1.4,0.7-1.7l5.3-2.2c0.7-0.3,1.5,0,1.8,0.7S84.9,71.6,84.2,71.9"/>
                                <path d="M73.7,76.3l-5.3,2.2c-0.7,0.3-1.5,0-1.7-0.7c-0.3-0.7,0-1.4,0.7-1.7l5.3-2.2c0.7-0.3,1.5,0,1.8,0.7
                                    C74.6,75.3,74.3,76,73.7,76.3"/>
                                <path d="M63.1,80.7L57.8,83c-0.7,0.3-1.5,0-1.7-0.7s0-1.4,0.7-1.7l5.3-2.2c0.7-0.3,1.5,0,1.7,0.7S63.8,80.5,63.1,80.7"/>
                                <path d="M52.6,85.2l-5.3,2.2c-0.7,0.3-1.5,0-1.7-0.7c-0.3-0.7,0-1.4,0.7-1.7l5.3-2.2c0.7-0.3,1.5,0,1.7,0.7
                                    C53.6,84.1,53.2,84.9,52.6,85.2"/>
                                <path d="M42,89.6l-5.3,2.2c-0.7,0.3-1.5,0-1.7-0.7c-0.3-0.7,0-1.4,0.7-1.7l5.3-2.2c0.7-0.3,1.5,0,1.7,0.7
                                    C43,88.6,42.7,89.3,42,89.6"/>
                                <path d="M31.5,94l-5.3,2.2c-0.7,0.3-1.5,0-1.8-0.7c-0.3-0.7,0-1.4,0.7-1.7l5.3-2.2c0.7-0.3,1.4,0,1.7,0.7
                                    C32.4,93,32.1,93.7,31.5,94"/>
                                <path d="M102.5,64.2l-2.5,1c-0.7,0.3-1.5,0-1.7-0.7c-0.3-0.7,0-1.4,0.7-1.7l2.5-1c0.7-0.3,1.5,0,1.8,0.7
                                    C103.5,63.2,103.2,63.9,102.5,64.2"/>
                            </g>
                            <path d="M103.2,62.5c-4-8.9-26.1-7.9-50.4,2.3C28.6,75,12.7,89.9,16.7,98.8c4,8.9,26.1,7.9,50.4-2.3
                                C91.3,86.4,107.2,71.4,103.2,62.5z M66,94.2c-24.4,10.2-43.9,10.1-46.9,3.6c-2.9-6.5,10.3-20.4,34.8-30.6
                                c24.4-10.2,44-10.1,46.9-3.6C103.7,70.1,90.4,83.9,66,94.2z"/>
                            <g>
                                <path d="M26,4.9c-0.7,0.3-1.5,0-1.7-0.7l-1.1-2.4c-0.3-0.7,0-1.4,0.7-1.7s1.5,0,1.7,0.7l1.1,2.4C27,3.8,26.7,4.6,26,4.9"/>
                                <path d="M57.2,74.5c-0.7,0.3-1.5,0-1.7-0.7l-2.2-5c-0.3-0.7,0-1.4,0.7-1.7c0.7-0.3,1.5,0,1.7,0.7l2.2,5
                                    C58.1,73.4,57.8,74.2,57.2,74.5"/>
                                <path d="M52.7,64.5c-0.7,0.3-1.5,0-1.7-0.7l-2.2-5c-0.3-0.7,0-1.4,0.7-1.7c0.7-0.3,1.5,0,1.7,0.7l2.2,5
                                    C53.7,63.5,53.4,64.3,52.7,64.5"/>
                                <path d="M48.3,54.6c-0.7,0.3-1.5,0-1.7-0.7l-2.2-5c-0.3-0.7,0-1.4,0.7-1.7c0.7-0.3,1.5,0,1.8,0.7l2.2,5
                                    C49.2,53.6,48.9,54.3,48.3,54.6"/>
                                <path d="M43.8,44.7c-0.7,0.3-1.5,0-1.7-0.7l-2.2-5c-0.3-0.7,0-1.4,0.7-1.7c0.7-0.3,1.5,0,1.7,0.7l2.2,5
                                    C44.8,43.6,44.5,44.4,43.8,44.7"/>
                                <path d="M39.3,34.7c-0.7,0.3-1.5,0-1.7-0.7l-2.2-5c-0.3-0.7,0-1.4,0.7-1.7c0.7-0.3,1.5,0,1.8,0.7l2.2,5
                                    C40.3,33.7,40,34.4,39.3,34.7"/>
                                <path d="M34.9,24.8c-0.7,0.3-1.5,0-1.7-0.7l-2.2-5c-0.3-0.7,0-1.4,0.7-1.7s1.5,0,1.7,0.7l2.2,5C35.9,23.7,35.6,24.5,34.9,24.8"/>
                                <path d="M30.4,14.8c-0.7,0.3-1.5,0-1.7-0.7l-2.2-5c-0.3-0.7,0-1.4,0.7-1.7c0.7-0.3,1.5,0,1.8,0.7l2.2,5
                                    C31.4,13.8,31.1,14.5,30.4,14.8"/>
                                <path d="M60.5,81.9c-0.7,0.3-1.5,0-1.7-0.7l-1.1-2.4c-0.3-0.7,0-1.4,0.7-1.7c0.7-0.3,1.5,0,1.7,0.7l1.1,2.4
                                    C61.5,80.8,61.1,81.6,60.5,81.9"/>
                            </g>
                            <path d="M67.7,91.4l-0.8,0.3c-0.1,0.1-0.3,0.1-0.3,0c-0.1,0-0.2-0.1-0.4-0.2l-1.8-1.3L64,92.6c0,0.2-0.1,0.3-0.1,0.4
                                c0,0.1-0.1,0.1-0.2,0.2L63,93.4c-0.1,0.1-0.2,0.1-0.2,0c0-0.1,0-0.2,0-0.4l0.6-3.3l-3-2c-0.1-0.1-0.2-0.2-0.2-0.2
                                c0,0,0.1-0.1,0.2-0.2l0.8-0.3c0.1,0,0.2,0,0.3,0c0.1,0,0.2,0.1,0.3,0.2l1.8,1.2l0.3-2.1c0-0.2,0-0.3,0.1-0.4c0-0.1,0.1-0.2,0.2-0.2
                                l0.7-0.3c0.2-0.1,0.3-0.1,0.3,0c0,0.1,0,0.2,0,0.3L64.6,89l3,2c0.1,0.1,0.2,0.2,0.2,0.3C67.9,91.3,67.8,91.4,67.7,91.4"/>
                            <path d="M6,51.5c0,0.1,0,0.2-0.1,0.3c0,0.1-0.1,0.1-0.2,0.2l-0.4,0.2c-0.2,0.1-0.3,0-0.3-0.2l-0.1-2.3l-4.8-4.8
                                c-0.1-0.1-0.1-0.3,0.1-0.4l0.9-0.4c0.1,0,0.2,0,0.2,0c0,0,0.1,0.1,0.1,0.2l3.4,3.5L4.6,43c0-0.1,0-0.1,0.1-0.2
                                c0-0.1,0.1-0.1,0.1-0.2l0.6-0.3c0.1-0.1,0.2-0.1,0.3,0c0,0,0.1,0.1,0,0.2L6,51.5z"/>
                            <g>
                                <path d="M105.9,68l-86.1,36.1c-0.4,0.2-0.8,0-0.9-0.4c-0.2-0.3,0-0.7,0.4-0.9l86.1-36.1c0.4-0.2,0.8,0,0.9,0.3
                                    C106.4,67.5,106.2,67.9,105.9,68"/>
                                <path d="M103.7,70.6l3.2-3.7c0.2-0.2,0.1-0.5-0.2-0.6l-5.3-0.6c-0.3,0-0.5,0.3-0.4,0.5l2.1,4.3C103.3,70.7,103.6,70.8,103.7,70.6"
                                    />
                                <path d="M20.9,100.1l-3.2,3.7c-0.2,0.2-0.1,0.5,0.2,0.6l5.3,0.6c0.3,0,0.5-0.3,0.4-0.5l-2.1-4.3C21.4,100,21.1,99.9,20.9,100.1"/>
                            </g>
                            <g>
                                <path d="M8.4,94.8c-0.1,0-0.2,0.1-0.3,0c-0.4,0-0.7-0.4-0.6-0.7l7.7-90.9c0-0.4,0.4-0.6,0.8-0.6c0.4,0,0.7,0.4,0.6,0.7L8.8,94.2
                                    C8.8,94.5,8.6,94.7,8.4,94.8"/>
                                <path d="M13,6.7l5.3,0.7c0.3,0,0.6-0.3,0.4-0.6l-2.5-5.1c-0.1-0.3-0.5-0.3-0.7,0l-2.8,4.4C12.5,6.3,12.7,6.6,13,6.7"/>
                                <path d="M11.3,90.9L6,90.2c-0.3,0-0.5,0.3-0.4,0.6l2.5,5.1c0.1,0.3,0.5,0.3,0.7,0l2.8-4.4C11.8,91.3,11.6,91,11.3,90.9"/>
                            </g>
                        </svg>
                        <div class="header__info">
                            <?php
                                $pageInfo['name'] = str_replace('w ', 'w&nbsp;', $pageInfo['name']);
                                $pageInfo['name'] = str_replace('im ', 'im&nbsp;', $pageInfo['name']);
                            ?>
                            <p class="header__title"><?php echo $pageInfo['name']; ?></p>
                            <div class="header__address">
                                <?php
                                    echo $headerAddress;
                                    if ($pageInfo['email'] != '') {
                                        echo '<a href="mailto:' . $pageInfo['email'] . '">' . $pageInfo['email'] . '</a>';
                                    }
                                ?>
                            </div>
                        </div>
                        <div id="banner" class="banner">
                            <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" viewBox="0 0 1164 459" class="banner-outer">
                                <path fill-rule="evenodd" stroke-width="5px" d="M153.727,454.500 C341.849,392.008 477.529,214.602 477.529,5.500 C477.529,5.166 477.524,4.833 477.523,4.500 L1149.500,4.500 C1155.023,4.500 1159.500,8.977 1159.500,14.500 L1159.500,454.500 L153.727,454.500 Z"/>
                            </svg>
                            <?php if ($outBannerTopRows == 0): ?>
                                <?php
                                    $img = $templateDir . '/' . 'images' . '/' . 'banner-default.jpg';
                                ?>
                                <div class="carousel-content">
                                    <div class="banner-photo">
                                        <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" viewBox="0 0 1164 459">
                                            <defs>
                                                <pattern id="banner-image" patternUnits="objectBoundingBox" width="100%" height="100%">
                                                    <image height="100%" width="100%" preserveAspectRatio="xMidYMid slice" xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="<?php echo $img; ?>"></image>
                                                </pattern>
                                            </defs>
                                            <path fill-rule="evenodd" fill="url(#banner-image)" d="M153.727,454.500 C341.849,392.008 477.529,214.602 477.529,5.500 C477.529,5.166 477.524,4.833 477.523,4.500 L1149.500,4.500 C1155.023,4.500 1159.500,8.977 1159.500,14.500 L1159.500,454.500 L153.727,454.500 Z"/>
                                        </svg>
                                    </div>
                                </div>
                            <?php elseif ($outBannerTopRows == 1): ?>
                                <?php
                                    $value = $outBannerTop[0];
                                    $img = $templateDir . '/' . 'images' . '/' . 'banner-default.jpg';
                                ?>
                                <div class="carousel-content">
                                    <div class="banner-photo">
                                        <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" viewBox="0 0 1164 459">
                                            <defs>
                                                <pattern id="banner-image" patternUnits="objectBoundingBox" width="100%" height="100%">
                                                    <image height="100%" width="100%" preserveAspectRatio="xMidYMid slice" xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="<?php echo $img; ?>"></image>
                                                </pattern>
                                            </defs>
                                            <path fill-rule="evenodd" fill="url(#banner-image)" d="M153.727,454.500 C341.849,392.008 477.529,214.602 477.529,5.500 C477.529,5.166 477.524,4.833 477.523,4.500 L1149.500,4.500 C1155.023,4.500 1159.500,8.977 1159.500,14.500 L1159.500,454.500 L153.727,454.500 Z"/>
                                        </svg>
                                    </div>
                                </div>
                            <?php else: $i; ?>
                                <div class="carousel-content owl-carousel">
                                    <?php foreach ($outBannerTop as $value): ?>
                                        <?php
                                            $i++;
                                            $img = $templateDir . '/' . 'images' . '/' . 'banner-default.jpg';
                                        ?>
                                        <div class="banner-photo">
                                            <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" viewBox="0 0 1164 459">
                                                <defs>
                                                    <pattern id="<?php echo 'banner-image-' . $i; ?>" patternUnits="objectBoundingBox" width="100%" height="100%">
                                                        <image height="100%" width="100%" preserveAspectRatio="xMidYMid slice" xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="<?php echo $img; ?>"></image>
                                                    </pattern>
                                                </defs>
                                                <path fill-rule="evenodd" fill="<?php echo 'url(#banner-image-' . $i . ')'; ?>" d="M153.727,454.500 C341.849,392.008 477.529,214.602 477.529,5.500 C477.529,5.166 477.524,4.833 477.523,4.500 L1149.500,4.500 C1155.023,4.500 1159.500,8.977 1159.500,14.500 L1159.500,454.500 L153.727,454.500 Z"/>
                                            </svg>
                                        </div>
                                    <?php endforeach ?>
                                </div>
                            <?php endif ?>
                        </div>
                        <div class="header__holder--mobile"></div>
                    </div>
                </header>
                <a id="main-menu" tabindex="-1" style="display: inline;"></a>
                <nav id="menu-top" class="menu-top">
                    <p class="sr-only"><?php echo __('main menu'); ?></p>
                    <div class="navbar-header">
                        <button class="navbar-toggle collapsed" aria-controls="navbar-top" aria-expanded="false" data-target="#navbar-top" id="navbar-mobile" data-toggle="collapse" type="button">
                            <i class="icon" aria-hidden="true"></i>
                            <i class="icon" aria-hidden="true"></i>
                            <i class="icon" aria-hidden="true"></i>
                            <span class="sr-only"><?php echo __('expand')?></span>
                            <span class="title sr-only"><?php echo __('main menu')?></span>
                        </button>
                    </div>
                    <div id="navbar-top" class="navbar-collapse menu">
                        <?php get_menu_tree('tm', 0, 0, '', false, '', false, true, true, false, true, true, '', true); ?>    
                    </div>
                </nav>
                <div class="page-content page-content-mobile">
                    <div class="container">
                        <div class="row">
                            <div class="col-xs-12">
                                <div id="content-mobile"></div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-xs-12">
                                <div id="sidebar-menu-mobile" class="sidebar"></div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
