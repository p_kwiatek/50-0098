<?php
if ($pagination['end'] > 1)
{
    ?>
<div class="row">
    <div class="pagination-wrapper col-xs-12">
        <?php
        $active_page = $pagination['active'];

        if ( !isset($pagination['active']) )
        {
            $active_page = 1;
        }
        ?>
        <p><?php echo __('page')?>: <strong><?php echo $active_page?></strong>/<?php echo $pagination['end']?></p>
        <ul class="list-unstyled list-inline text-center">
        <?php
        if ($pagination['start'] != $pagination['prev'])
        {
            ?>
            <li>
                <a href="<?php echo $url . $pagination['start']?>" rel="nofollow" class="btn-first">
                    <span class="sr-only"><?php echo __('first page')?></span>
                    <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="14px" height="10px">
                        <defs>
                            <filter filterUnits="userSpaceOnUse" id="pagination-first-filter" x="0px" y="0px" width="14px" height="10px">
                                <feOffset in="SourceAlpha" dx="0" dy="1" />
                                <feGaussianBlur result="blurOut" stdDeviation="0" />
                                <feFlood flood-color="rgb(255, 255, 255)" result="floodOut" />
                                <feComposite operator="atop" in="floodOut" in2="blurOut" />
                                <feComponentTransfer><feFuncA type="linear" slope="0.59"/></feComponentTransfer>
                                <feMerge>
                                    <feMergeNode/>
                                    <feMergeNode in="SourceGraphic"/>
                                </feMerge>
                            </filter>
                        </defs>
                        <g filter="url(#pagination-first-filter)">
                            <path fill-rule="evenodd" d="M13.702,4.521 L6.001,8.968 L6.001,5.503 L0.001,8.968 L0.001,0.075 L6.001,3.539 L6.001,0.075 L13.702,4.521 Z"/>
                        </g>
                    </svg>
                </a>
            </li>
            <li>
                <a href="<?php echo $url . $pagination['prev']?>" rel="nofollow" class="btn-prev">
                    <span class="sr-only"><?php echo __('prev page')?></span>
                    <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="8px" height="10px">
                        <defs>
                            <filter filterUnits="userSpaceOnUse" id="pagination-prev-filter" x="0px" y="0px" width="8px" height="10px"  >
                                <feOffset in="SourceAlpha" dx="0" dy="1" />
                                <feGaussianBlur result="blurOut" stdDeviation="0" />
                                <feFlood flood-color="rgb(255, 255, 255)" result="floodOut" />
                                <feComposite operator="atop" in="floodOut" in2="blurOut" />
                                <feComponentTransfer><feFuncA type="linear" slope="0.59"/></feComponentTransfer>
                                <feMerge>
                                    <feMergeNode/>
                                    <feMergeNode in="SourceGraphic"/>
                                </feMerge>
                            </filter>
                        </defs>
                        <g filter="url(#pagination-prev-filter)">
                            <path fill-rule="evenodd" d="M7.702,4.521 L0.001,8.968 L0.001,0.075 L7.702,4.521 Z"/>
                        </g>
                    </svg>
                </a>
            </li>
            <?php
        }		
        foreach ($pagination as $k => $v)
        {
            if (is_numeric($k))
            {
                ?>
            <li>
                <a href="<?php echo $url . $v?>" rel="nofollow" class="btn-page">
                    <span class="sr-only"><?php echo __('page')?></span>
                    <?php echo $v?>
                </a>
            </li>
                <?php
            } else if ($k == 'active')
            {
                ?>
            <li>
                <span class="page-active">
                    <span class="sr-only"><?php echo __('page')?></span>
                    <?php echo $v?>
                </span>
            </li>
                <?php
            }			
        }		
        if ($pagination['active'] != $pagination['end'])
        {
            ?>
            <li>
                <a href="<?php echo $url . $pagination['next']?>" rel="nofollow" class="btn-next">
                    <span class="sr-only"><?php echo __('next page')?></span>
                    <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="8px" height="10px">
                        <defs>
                            <filter filterUnits="userSpaceOnUse" id="pagination-next-filter" x="0px" y="0px" width="8px" height="10px"  >
                                <feOffset in="SourceAlpha" dx="0" dy="1" />
                                <feGaussianBlur result="blurOut" stdDeviation="0" />
                                <feFlood flood-color="rgb(255, 255, 255)" result="floodOut" />
                                <feComposite operator="atop" in="floodOut" in2="blurOut" />
                                <feComponentTransfer><feFuncA type="linear" slope="0.59"/></feComponentTransfer>
                                <feMerge>
                                    <feMergeNode/>
                                    <feMergeNode in="SourceGraphic"/>
                                </feMerge>
                            </filter>
                        </defs>
                        <g filter="url(#pagination-next-filter)">
                            <path fill-rule="evenodd" d="M7.702,4.521 L0.001,8.968 L0.001,0.075 L7.702,4.521 Z"/>
                        </g>
                    </svg>
                </a>
            </li>
            <li>
                <a href="<?php echo $url . $pagination['end']?>" rel="nofollow" class="btn-last">
                    <span class="sr-only"><?php echo __('last page')?></span>
                    <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="14px" height="10px">
                        <defs>
                            <filter filterUnits="userSpaceOnUse" id="pagination-last-filter" x="0px" y="0px" width="14px" height="10px">
                                <feOffset in="SourceAlpha" dx="0" dy="1" />
                                <feGaussianBlur result="blurOut" stdDeviation="0" />
                                <feFlood flood-color="rgb(255, 255, 255)" result="floodOut" />
                                <feComposite operator="atop" in="floodOut" in2="blurOut" />
                                <feComponentTransfer><feFuncA type="linear" slope="0.59"/></feComponentTransfer>
                                <feMerge>
                                    <feMergeNode/>
                                    <feMergeNode in="SourceGraphic"/>
                                </feMerge>
                            </filter>
                        </defs>
                        <g filter="url(#pagination-last-filter)">
                            <path fill-rule="evenodd" d="M13.702,4.521 L6.001,8.968 L6.001,5.503 L0.001,8.968 L0.001,0.075 L6.001,3.539 L6.001,0.075 L13.702,4.521 Z"/>
                        </g>
                    </svg>
                </a>
            </li>
            <?php
        }
        ?>	
        </ul>
    </div>
</div>
    <?php
}
?>