<div class="main-text">
<h2 class="main-header"><span><?php echo $pageName?></span></h2>
<?php
echo $message;

if ($showLoginForm)
{
    include( CMS_TEMPL . DS . 'form_login.php');
}
	
if ($showPage)
{
    echo $row['text'];
		
    if (! check_html_text($row['author'], '') )
    {
	?>
	<div class="authorName"><?php echo __('author'); ?>: <span><?php echo $row['author']?></span></div>
	<?php
    }
			
    /*
     * Articles
     */
    if ($numArticles > 0)
    {	
	$i = 0;
	?>		
	<div class="article-wrapper">
	    <?php
	    foreach ($outRowArticles as $row)
            {
		$highlight = $url = $target = $url_title = $protect = '';
			
		if ($row['protected'] == 1)
		{
        $protect = '<span class="protectedPage"></span>';
        $url_title = ' title="' . __('page requires login') . '"';
		}				
				
		if (trim($row['ext_url']) != '')
		{
		    if ($row['new_window'] == '1')
		    {
			$target = ' target="_blank"';
		    }	
		    $url_title = ' title="' . __('opens in new window') . '"';
		    $url = ref_replace($row['ext_url']);					
		} else
		{
		    if ($row['url_name'] != '')
		    {
			$url = 'art,' . $row['id_art'] . ',' . $row['url_name'];
		    } else
		    {
			$url = 'index.php?c=article&amp;id=' . $row['id_art'];
		    }
		}	
				
		$margin = ' no-photo';
		if (is_array($photoLead[$row['id_art']]))
		{
		    $margin = '';
		}				
				
		$row['show_date'] = substr($row['show_date'], 0, 10);
		
		$highlight = '';
		if ($row['highlight'] == 1)
		{
		    $highlight = ' highlight-article';
		}			
		?>
        <div class="article<?php echo $highlight?><?php if (!is_array($photoLead[$row['id_art']])): ?> no-photo<?php endif; ?>">
            <div class="article__decoration">
                <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" class="ribbon" width="24px" height="46px">
                    <defs>
                        <filter filterUnits="userSpaceOnUse" id="<?php echo 'article-ribbon-filter-' . $i; ?>" x="0px" y="0px" width="24px" height="46px">
                            <feOffset in="SourceAlpha" dx="0" dy="1" />
                            <feGaussianBlur result="blurOut" stdDeviation="0" />
                            <feFlood flood-color="rgb(0, 0, 0)" result="floodOut" />
                            <feComposite operator="atop" in="floodOut" in2="blurOut" />
                            <feComponentTransfer><feFuncA type="linear" slope="0.2"/></feComponentTransfer>
                            <feMerge>
                                <feMergeNode/>
                                <feMergeNode in="SourceGraphic"/>
                            </feMerge>
                        </filter>
                    </defs>
                    <g filter="<?php echo 'url(#article-ribbon-filter-' . $i . ')'; ?>">
                        <path fill-rule="evenodd" d="M24.003,0.937 L24.003,44.984 L11.941,34.950 L-0.005,45.015 L-0.005,0.937 L24.003,0.937 Z"/>
                    </g>
                </svg>
                <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" class="star" width="16px" height="17px">
                    <defs>
                        <filter filterUnits="userSpaceOnUse" id="<?php echo 'article-star-filter-' . $i; ?>" x="0px" y="0px" width="16px" height="17px">
                            <feOffset in="SourceAlpha" dx="0" dy="1" />
                            <feGaussianBlur result="blurOut" stdDeviation="0" />
                            <feFlood flood-color="rgb(255, 255, 255)" result="floodOut" />
                            <feComposite operator="atop" in="floodOut" in2="blurOut" />
                            <feComponentTransfer><feFuncA type="linear" slope="0.59"/></feComponentTransfer>
                            <feMerge>
                                <feMergeNode/>
                                <feMergeNode in="SourceGraphic"/>
                            </feMerge>
                        </filter>
                    </defs>
                    <g filter="<?php echo 'url(#article-star-filter-' . $i . ')'; ?>">
                        <path fill-rule="evenodd" d="M8.000,0.228 L9.993,5.868 L15.973,6.021 L11.225,9.660 L12.928,15.394 L8.000,12.003 L3.072,15.394 L4.775,9.660 L0.027,6.021 L6.007,5.868 L8.000,0.228 Z"/>
                    </g>
                </svg>
            </div>
            <?php
                if (is_array($photoLead[$row['id_art']]))
                {
                    $photo = $photoLead[$row['id_art']];
                    ?>
                    <div class="photo-wrapper<?php echo $photoWrapper; ?>">
                        <a href="files/<?php echo $lang?>/<?php echo $photo['file']?>" title="<?php echo __('enlarge image') . ': ' . $row['name']?>" class="photo fancybox" data-fancybox-group="gallery">
                            <span class="sr-only"><?php echo __('enlarge image')?>: <?php echo $row['name']?></span>
                            <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" viewBox="0 0 369 239">
                                <defs>
                                    <pattern id="<?php echo 'article-image-' . $i; ?>" patternUnits="objectBoundingBox" width="100%" height="100%">
                                        <image height="100%" width="100%" preserveAspectRatio="xMidYMid slice" xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="files/<?php echo $lang?>/mini/<?php echo $photo['file']; ?>"></image>
                                    </pattern>
                                </defs>
                                <path fill-rule="evenodd" stroke-width="5px" fill="<?php echo 'url(#article-image-' . $i . ')'; ?>" d="M28.534,234.500 C28.534,234.485 28.537,234.470 28.537,234.455 C28.537,221.171 17.779,210.401 4.500,210.377 L4.500,4.500 L340.377,4.500 C340.401,17.779 351.171,28.536 364.455,28.536 C364.470,28.536 364.485,28.534 364.500,28.534 L364.500,234.500 L28.534,234.500 Z"/>
                            </svg>
                        </a>
                    </div>
                    <?php
                }   
            ?>
            <div class="lead-text<?php echo $margin; ?>">
                <h4 class="article-title <?php echo $margin?>">
                    <a href="<?php echo $url?>" <?php echo $url_title . $target ?>>
                        <span><?php echo $row['name'] . $protect?></span>
                    </a>
                </h4>
                <div class="lead-main-text">
                    <?php echo truncate_html($row['lead_text'], 300, '...')?>
                </div>
                <div class="article-meta">
                    <a href="<?php echo $url ?>" <?php echo $url_title . $target ?> class="button">
                        <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="8px" height="10px">
                            <defs>
                                <filter filterUnits="userSpaceOnUse" id="<?php echo 'article-arrow-filter-' . $i; ?>" x="0px" y="0px" width="8px" height="10px">
                                    <feOffset in="SourceAlpha" dx="0" dy="1" />
                                    <feGaussianBlur result="blurOut" stdDeviation="0" />
                                    <feFlood flood-color="rgb(255, 255, 255)" result="floodOut" />
                                    <feComposite operator="atop" in="floodOut" in2="blurOut" />
                                    <feComponentTransfer><feFuncA type="linear" slope="0.59"/></feComponentTransfer>
                                    <feMerge>
                                        <feMergeNode/>
                                        <feMergeNode in="SourceGraphic"/>
                                    </feMerge>
                                </filter>
                            </defs>
                            <g filter="<?php echo 'url(#article-arrow-filter-' . $i . ')'; ?>">
                                <path fill-rule="evenodd" d="M7.702,4.521 L0.001,8.968 L0.001,0.075 L7.702,4.521 Z"/>
                            </g>
                        </svg>
                        <span><?php echo __('read more') ?></span>
                        <span class="sr-only"> <?php echo __('about')?>: <?php echo $row['name']; ?></span>
                    </a>
                    <?php if ($row['show_date'] != '' && $row['show_date'] != '0000-00-00') { ?>
                        <p class="article-date">
                            <span><?php echo $row['show_date'] ?></span>
                        </p>
                    <?php } ?>
                </div>
            </div>
	    </div>
            <?php		
	    }
	    ?>
	    	
        <?php

	$url = $PHP_SELF.'?c=' . $_GET['c'] . '&amp;id=' . $_GET['id'] . '&amp;s=';
	include (CMS_TEMPL . DS . 'pagination.php');
	?>
	</div>
	<?php
    }		
				
        /*
	 * Wypisanie plikow do pobrania
	 */
    if ($numFiles > 0)
    {	
    ?>
	<div class="files-wrapper">
            
                <h3 class="files-header"><span><?php echo __('files')?></span></h3>
                <ul class="list-unstyled">
                <?php
                foreach ($outRowFiles as $row)
                {
                    $target = 'target="_blank" ';

                    if (filesize('download/'.$row['file']) > 5000000)
                    {
                        $url = 'download/'.$row['file'];
                    } else
                    {
                        $url = 'index.php?c=getfile&amp;id='.$row['id_file'];
                    }
                    if (trim($row['name']) == '')
                    {
                        $name = $row['file'];
                    } else
                    {
                        $name = $row['name'];
                    }

                    $size = file_size('download/'.$row['file']);	
                    ?>		
                    <li>
                        <a href="<?php echo $url?>" <?php echo $target?>>
                            <i class="icon-doc-text-inv icon" aria-hidden="true"></i>
                            <span class="title">
                                <?php echo $name?>
                                <span class="size">(<?php echo $size?>)</span>
                            </span>                        
                        </a>                        
                    </li>
                    <?php
                }
                ?>
                </ul>
            
	</div>
    <?php
    }
		
    /*
     *  Wypisanie zdjec
     */
    if ($numPhotos > 0)
    {	
	$i = 0;
	?>
	<div class="gallery-wrapper">
        <h3 class="gallery-header"><span><?php echo __('gallery')?></span></h3>
        <ul class="gallery">
            <?php
            foreach ($outRowPhotos as $row)
            {
                $i++;
                ?>
                <li>
                    <a href="files/<?php echo $lang?>/<?php echo $row['file']?>" title="<?php echo __('enlarge image') . ': ' . $row['name']?>" data-fancybox-group="gallery" class="photo fancybox">
                        <span class="sr-only"><?php echo __('enlarge image')?>: <?php echo $row['name']?></span>
                        <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" viewBox="0 0 369 239">
                            <defs>
                                <pattern id="<?php echo 'article-image-' . $i; ?>" patternUnits="objectBoundingBox" width="100%" height="100%">
                                    <image height="100%" width="100%" preserveAspectRatio="xMidYMid slice" xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="files/<?php echo $lang?>/mini/<?php echo $row['file']; ?>"></image>
                                </pattern>
                            </defs>
                            <path fill-rule="evenodd" stroke-width="5px" fill="<?php echo 'url(#article-image-' . $i . ')'; ?>" d="M28.534,234.500 C28.534,234.485 28.537,234.470 28.537,234.455 C28.537,221.171 17.779,210.401 4.500,210.377 L4.500,4.500 L340.377,4.500 C340.401,17.779 351.171,28.536 364.455,28.536 C364.470,28.536 364.485,28.534 364.500,28.534 L364.500,234.500 L28.534,234.500 Z"/>
                        </svg>
                        <?php
                        if (! check_html_text($row['name'], '') ) {
                            ?>
                            <p class="photo-name" aria-hidden="true"><?php echo $row['name']?></p>
                            <?php
                        }
                        ?>
                    </a>
                </li>
                <?php
            }
            ?>
        </ul>
	</div>
	<?php
    }		

    if ($outSettings['pluginTweet'] == 'włącz')
    {
	 echo '<div class="Tweet"><iframe frameborder="0" scrolling="no" src="//platform.twitter.com/widgets/tweet_button.html" style="width:80px; height:30px;"></iframe></div>';  
    }

    if ($outSettings['pluginFB'] == 'włącz')
    {
	$fb_url = urlencode('http://'.$pageInfo['host'].'/index.php?c=page&amp&id='. $_GET['id']);
	echo '<div class="FBLike"><iframe src=\'http://www.facebook.com/plugins/like.php?href='.$fb_url.'&amp;layout=standard&amp;show_faces=true&amp;width=400&amp;action=like&amp;font=tahoma&amp;colorscheme=light&amp;height=32&amp;show_faces=false\' scrolling="no" frameborder="0" style="border:none; overflow:hidden; width:400px; height:32px;"></iframe></div>';   
}
}
?>
</div>