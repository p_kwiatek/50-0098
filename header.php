<!DOCTYPE html>
<html lang="<?php echo $lang; ?>">
<!--[if lte IE 8]>
<script type="text/javascript">
    window.location = "<?php echo $pathTemplate?>/ie8.php";
</script>
<![endif]-->
<head>
<meta charset="UTF-8" />
<title><?php echo $pageTitle; ?></title>
<meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=yes">
<meta name="description" content="<?php echo $pageDescription; ?>" />
<meta name="keywords" content="<?php echo $pageKeywords; ?>" />
<meta name="author" content="<?php echo $cmsConfig['cms']; ?>" />
<meta name="revisit-after" content="3 days" />
<meta name="robots" content="all" />
<meta name="robots" content="index, follow" />
<?php

array_unshift($css, 'vendor/bootstrap/css/bootstrap.min.css');

$jquery = array_shift($js);
array_unshift($js, 'owl.carousel.min.js');
array_unshift($js, 'ismobile.js');
array_unshift($js, 'modernizr.js');
array_unshift($js, 'what-input.min.js');
array_unshift($js, 'vendor/snap/snap.svg.min.js');
array_unshift($js, 'vendor/bootstrap/js/bootstrap.min.js');
array_unshift($js, $jquery);

$pathTemplate = 'http://' . $pageInfo['host'] . '/' . $templateDir;
	
foreach ($js as $k => $v) {
    if (strpos($v, 'vendor') !== FALSE) {
        $path = $pathTemplate .'/' . $v;
    } else {
        $path = $pathTemplate .'/js/' . $v;
    }
    
    echo '<script type="text/javascript" src="'. $path . '"></script>' . "\r\n";
    
}
?>

<?php
foreach ($css as $k => $v) {
    if (strpos($v, 'vendor') !== FALSE) {
        echo '<link rel="stylesheet" media="all" type="text/css" href="'. $pathTemplate .'/' . $v . '"/>' . "\r\n";
    } else {
        echo '<link rel="stylesheet" media="all" type="text/css" href="'. $pathTemplate .'/css/' . $v . '"/>' . "\r\n";
    }
    
    if ($v == 'style.css')
    {
	$popupBackground = '#fff';
        $mainColor = $templateConfig['mainColor'];
        $overlayColor = $templateConfig['overColor'];
        $highColor = $templateConfig['highColor'];
			
	// wersja zalobna
	if ($outSettings['funeral'] == 'włącz') 
	{
	    $popupBackground = $templateConfig['popupBackground-bw'];
	    $mainColor = $templateConfig['mainColor-bw'];
	    $overlayColor = $templateConfig['overColor-bw'];
	    $highColor = $templateConfig['highColor-bw'];
	    echo '<link rel="stylesheet" media="all" type="text/css" href="'. $pathTemplate .'/bw/css/style.css"/>' . "\r\n";
	}		
    }
		
    if ($v == 'jquery.fancybox.css')
    {
        if ($outSettings['funeral'] == 'włącz') 
        {
	    echo '<link rel="stylesheet" media="all" type="text/css" href="'. $pathTemplate .'/bw/css/jquery.fancybox.css"/>' . "\r\n";
	}
    }
    if ($v == 'addition.css')
    {
	if ($outSettings['funeral'] == 'włącz')
	{
	    echo '<link rel="stylesheet" media="all" type="text/css" href="'. $pathTemplate .'/bw/css/addition.css"/>' . "\r\n";
	}
    }      
}

$contrast = '';
if ($_SESSION['contr'] == 1)
{
    $contrast = 'flashvars.contrast = 1;' . "\r\n";
    $popupBackground = $templateConfig['popupBackground-ct'];
    $mainColor = $templateConfig['mainColor-ct'];
    $overlayColor = $templateConfig['overColor-ct'];
    $highColor = $templateConfig['highColor-ct'];	    
    echo '<link rel="stylesheet" media="all" type="text/css" href="'. $pathTemplate .'/contrast/css/style.css"/>' . "\r\n";
    echo '<link rel="stylesheet" media="all" type="text/css" href="'. $pathTemplate .'/contrast/css/jquery.fancybox.css"/>' . "\r\n";
    echo '<link rel="stylesheet" media="all" type="text/css" href="'. $pathTemplate .'/contrast/css/addition.css"/>' . "\r\n";
}	
	
echo '<link rel="shortcut icon" href="http://' . $pageInfo['host'] . '/' . $templateDir . '/images/favicon.ico" />' . "\r\n";

switch ($_SESSION['style']) {
    case '1' :
        ?>
        <link rel="stylesheet" media="all" type="text/css" href="<?php echo $pathTemplate?>/css/font-1.css"/>
        <?php
        break;
    case '2' :
        ?>
        <link rel="stylesheet" media="all" type="text/css" href="<?php echo $pathTemplate?>/css/font-2.css"/>
        <?php        
        break;
}
?>
<script type="text/javascript">
    texts = {
        image: '<?php echo __('image')?>',
        enlargeImage: '<?php echo __('enlarge image')?>',
        closeGallery: '<?php echo __('close gallery')?>',
        prevGallery: '<?php echo __('prev gallery')?>',
        nextGallery: '<?php echo __('next gallery')?>'
    };
    
    settings = {
        overlayColor: '<?php echo $overlayColor; ?>',
        transition: '<?php echo $outSettings['animType']?>',
        animationDuration: <?php echo $outSettings['transition']?>,
        duration: <?php echo $outSettings['duration']?>,
        showClock: false
    };
</script>
<script type="text/javascript">
    var _gaq = _gaq || [];
    _gaq.push(['_setAccount', 'UA-3924208-1']);
    _gaq.push(['_setDomainName', '.szkolnastrona.pl']);
    _gaq.push(['_setAllowHash', false]);
    _gaq.push(['_trackPageview']);

    (function() {
            var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
            ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
            var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
    })();
</script>
<?php 
    $icons = array(

        'mod_stats' => '<svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" class="icon-stats" width="33px" height="28px">
            <defs>
                <filter filterUnits="userSpaceOnUse" id="icon-stats-filter" x="0px" y="0px" width="33px" height="28px">
                    <feOffset in="SourceAlpha" dx="0" dy="0" />
                    <feGaussianBlur result="blurOut" stdDeviation="2.236" />
                    <feFlood flood-color="rgb(42, 87, 17)" result="floodOut" />
                    <feComposite operator="atop" in="floodOut" in2="blurOut" />
                    <feComponentTransfer><feFuncA type="linear" slope="1"/>
                    </feComponentTransfer>
                    <feMerge>
                        <feMergeNode/>
                        <feMergeNode in="SourceGraphic"/>
                    </feMerge>
                </filter>
            </defs>
            <path fill-rule="evenodd"  class="icon__path--1" d="M11.010,23.374 C11.010,24.349 10.106,25.154 8.981,25.154 L7.978,25.154 C6.866,25.154 5.956,24.349 5.956,23.374 L5.956,12.726 C5.956,11.752 6.866,10.959 7.978,10.959 L8.981,10.959 C10.106,10.959 11.010,11.752 11.010,12.726 L11.010,23.374 Z"/>
            <path fill-rule="evenodd"  class="icon__path--1" d="M18.010,23.374 C18.010,24.349 17.106,25.154 15.981,25.154 L14.978,25.154 C13.866,25.154 12.956,24.349 12.956,23.374 L12.956,8.726 C12.956,7.752 13.866,6.959 14.978,6.959 L15.981,6.959 C17.106,6.959 18.010,7.752 18.010,8.726 L18.010,23.374 Z"/>
            <path fill-rule="evenodd"  class="icon__path--1" d="M25.010,23.374 C25.010,24.349 24.106,25.154 22.981,25.154 L21.978,25.154 C20.866,25.154 19.956,24.349 19.956,23.374 L19.956,3.726 C19.956,2.752 20.866,1.959 21.978,1.959 L22.981,1.959 C24.106,1.959 25.010,2.752 25.010,3.726 L25.010,23.374 Z"/>
            <g filter="url(#icon-stats-filter)">
                <path fill-rule="evenodd"  class="icon__path--2" d="M27.024,11.740 L25.242,5.660 C25.155,5.351 24.941,5.107 24.676,4.967 C24.416,4.822 24.098,4.775 23.791,4.866 L17.711,6.642 C17.102,6.816 16.748,7.457 16.930,8.075 L17.165,8.872 C17.343,9.480 17.983,9.837 18.590,9.655 L20.544,9.082 C16.512,14.713 11.041,17.278 6.350,18.755 C5.503,19.004 4.862,19.683 4.977,20.513 C4.991,20.608 4.998,20.663 5.010,20.762 C5.125,21.592 6.047,22.327 6.942,21.991 C14.851,18.860 17.506,17.761 23.446,10.692 L24.009,12.619 C24.185,13.225 24.827,13.574 25.435,13.399 L26.241,13.163 C26.848,12.989 27.201,12.341 27.024,11.740 "/>
            </g>
        </svg>',
        'mod_kzk' => '<svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" class="icon-kzk" width="24px" height="30px">
            <defs>
                <filter filterUnits="userSpaceOnUse" id="icon-kzk-filter" x="0px" y="0px" width="24px" height="14px">
                    <feOffset in="SourceAlpha" dx="0" dy="0" />
                    <feGaussianBlur result="blurOut" stdDeviation="2.236" />
                    <feFlood flood-color="rgb(42, 87, 17)" result="floodOut" />
                    <feComposite operator="atop" in="floodOut" in2="blurOut" />
                    <feComponentTransfer><feFuncA type="linear" slope="1"/>
                    </feComponentTransfer>
                    <feMerge>
                        <feMergeNode/>
                        <feMergeNode in="SourceGraphic"/>
                    </feMerge>
                </filter>
                <filter filterUnits="userSpaceOnUse" id="icon-kzk-2-filter" x="3px" y="7px" width="16px" height="23px">
                <feOffset in="SourceAlpha" dx="0" dy="0" />
                    <feGaussianBlur result="blurOut" stdDeviation="2.236" />
                    <feFlood flood-color="rgb(42, 87, 17)" result="floodOut" />
                    <feComposite operator="atop" in="floodOut" in2="blurOut" />
                    <feComponentTransfer><feFuncA type="linear" slope="1"/>
                    </feComponentTransfer>
                    <feMerge>
                        <feMergeNode/>
                        <feMergeNode in="SourceGraphic"/>
                    </feMerge>
                </filter>
            </defs>
            <g filter="url(#icon-kzk-filter)">
                <path fill-rule="evenodd" class="icon__path--2" d="M17.495,8.964 C16.662,8.964 15.988,8.289 15.988,7.457 L15.988,5.538 C15.988,4.705 16.662,4.031 17.495,4.031 C18.327,4.031 19.002,4.705 19.002,5.538 L19.002,7.457 C19.002,8.289 18.327,8.964 17.495,8.964 ZM5.495,8.964 C4.662,8.964 3.988,8.289 3.988,7.457 L3.988,5.538 C3.988,4.705 4.662,4.031 5.495,4.031 C6.327,4.031 7.002,4.705 7.002,5.538 L7.002,7.457 C7.002,8.289 6.327,8.964 5.495,8.964 Z"/>
            </g>
            <path fill-rule="evenodd"  class="icon__path--1" d="M21.000,28.000 L2.000,28.000 C0.895,28.000 -0.000,27.104 -0.000,26.000 L-0.000,8.000 C-0.000,6.895 0.895,6.000 2.000,6.000 L3.036,6.000 L3.036,8.019 C3.036,9.097 4.158,9.971 5.542,9.971 C6.926,9.971 8.048,9.097 8.048,8.019 L8.048,6.000 L14.889,6.000 L14.889,8.019 C14.889,9.097 16.034,9.971 17.445,9.971 C18.857,9.971 20.002,9.097 20.002,8.019 L20.002,6.000 L21.000,6.000 C22.105,6.000 23.000,6.895 23.000,8.000 L23.000,26.000 C23.000,27.104 22.105,28.000 21.000,28.000 ZM21.031,13.969 L2.000,13.969 L2.000,23.968 L21.031,23.968 L21.031,13.969 Z"/>
            <g filter="url(#icon-kzk-2-filter)">
                <text  class="icon__path--2" x="6px" y="25.062px">&#49;</text>
            </g>
        </svg>',
        'mod_location' => '<svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" class="icon-location" width="40px" height="27px">
            <defs>
                <filter filterUnits="userSpaceOnUse" id="icon-location-filter" x="0px" y="1px" width="40px" height="18px">
                    <feOffset in="SourceAlpha" dx="0" dy="0" />
                    <feGaussianBlur result="blurOut" stdDeviation="2.236" />
                    <feFlood flood-color="rgb(42, 87, 17)" result="floodOut" />
                    <feComposite operator="atop" in="floodOut" in2="blurOut" />
                    <feComponentTransfer><feFuncA type="linear" slope="1"/></feComponentTransfer>
                    <feMerge>
                        <feMergeNode/>
                        <feMergeNode in="SourceGraphic"/>
                    </feMerge>
                </filter>
            </defs>
            <path fill-rule="evenodd" class="icon__path--3" d="M18.469,0.918 C19.295,0.918 19.966,1.589 19.966,2.416 L19.966,25.438 C19.966,26.264 19.295,26.935 18.469,26.935 C17.642,26.935 16.971,26.264 16.971,25.438 L16.971,2.416 C16.971,1.589 17.642,0.918 18.469,0.918 Z"/>
            <g filter="url(#icon-location-filter)">
                <path fill-rule="evenodd" class="icon__path--2" d="M33.546,9.727 L32.178,10.983 L20.975,10.983 L20.943,5.957 L32.146,5.957 L33.530,7.212 L34.912,8.468 L33.546,9.727 ZM7.666,13.984 L6.298,12.727 L4.931,11.468 L6.313,10.212 L7.697,8.956 L15.900,8.956 L15.869,13.984 L7.666,13.984 Z"/>
            </g>
        </svg>',
        'mod_menu' => '<svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" class="icon-menu" width="31px" height="37px">
            <defs>
                <filter filterUnits="userSpaceOnUse" id="icon-menu-filter" x="3px" y="0px" width="28px" height="30px">
                    <feOffset in="SourceAlpha" dx="0" dy="0" />
                    <feGaussianBlur result="blurOut" stdDeviation="2.236" />
                    <feFlood flood-color="rgb(42, 87, 17)" result="floodOut" />
                    <feComposite operator="atop" in="floodOut" in2="blurOut" />
                    <feComponentTransfer><feFuncA type="linear" slope="1"/></feComponentTransfer>
                    <feMerge>
                        <feMergeNode/>
                        <feMergeNode in="SourceGraphic"/>
                    </feMerge>
                </filter>
            </defs>
            <path fill-rule="evenodd" class="icon__path--3" d="M25.035,26.062 C25.035,25.981 25.034,25.900 25.034,25.819 L0.929,25.819 C0.927,25.900 0.924,25.981 0.924,26.062 C0.924,30.265 3.094,33.958 6.375,36.103 L19.582,36.103 C22.866,33.958 25.035,30.265 25.035,26.062 "/>
            <g filter="url(#icon-menu-filter)">
                <path fill-rule="evenodd" class="icon__path--2" d="M25.177,21.934 L19.943,24.989 L13.334,24.989 L23.500,19.058 C24.295,18.599 25.313,18.863 25.777,19.659 C26.239,20.452 25.972,21.472 25.177,21.934 ZM18.422,16.664 L18.415,16.686 L18.393,16.673 C18.309,16.695 18.228,16.708 18.140,16.708 C17.709,16.708 15.753,14.926 15.748,12.702 C15.734,10.262 18.258,9.252 18.258,9.252 C18.258,9.252 16.753,10.912 17.030,12.828 C17.224,14.224 18.709,15.537 18.709,15.537 C18.846,15.663 18.925,15.832 18.925,16.014 C18.925,16.313 18.716,16.565 18.422,16.664 ZM11.595,21.585 C11.595,21.585 13.278,18.997 12.876,15.646 C12.695,14.146 9.784,11.944 9.695,10.300 C9.558,7.600 14.004,4.851 14.004,4.851 C14.004,4.851 11.876,7.101 11.901,9.841 C11.918,11.245 14.503,13.556 14.567,14.965 C14.746,18.975 11.595,21.585 11.595,21.585 ZM8.916,20.483 C9.117,21.315 9.411,21.614 10.034,22.672 C10.401,23.292 9.644,24.033 9.644,24.033 C9.644,24.033 9.747,23.366 9.159,22.893 C8.754,22.564 7.507,22.026 7.637,19.395 C7.735,17.422 10.153,16.625 10.153,16.625 C10.153,16.625 8.450,18.484 8.916,20.483 Z"/>
            </g>
        </svg>',
        'mod_newsletter' => '<svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" class="icon-newsletter" width="26px" height="22px">
            <defs>
                <filter filterUnits="userSpaceOnUse" id="icon-newsletter-filter" x="4px" y="0px" width="19px" height="22px">
                    <feOffset in="SourceAlpha" dx="0" dy="0" />
                    <feGaussianBlur result="blurOut" stdDeviation="2.236" />
                    <feFlood flood-color="rgb(42, 87, 17)" result="floodOut" />
                    <feComposite operator="atop" in="floodOut" in2="blurOut" />
                    <feComponentTransfer><feFuncA type="linear" slope="1"/></feComponentTransfer>
                    <feMerge>
                        <feMergeNode/>
                        <feMergeNode in="SourceGraphic"/>
                    </feMerge>
                </filter>
            </defs>
            <path fill-rule="evenodd" class="icon__path--3" d="M25.922,18.904 C25.919,19.338 25.787,19.732 25.569,20.056 L16.633,12.466 L25.627,4.998 C25.668,5.061 25.708,5.126 25.742,5.192 C25.769,5.242 25.790,5.286 25.813,5.333 C25.927,5.593 25.991,5.880 25.990,6.185 L25.922,18.904 ZM0.803,4.479 C1.148,4.180 1.593,4.005 2.075,4.005 L24.026,4.134 C24.133,4.134 24.234,4.144 24.334,4.161 C24.594,4.202 24.841,4.300 25.057,4.441 C25.139,4.495 25.219,4.553 25.293,4.620 L12.992,11.838 L0.803,4.479 ZM0.360,19.912 C0.191,19.656 0.075,19.349 0.036,19.020 C0.024,18.932 0.018,18.847 0.021,18.757 L0.089,6.037 C0.090,5.759 0.144,5.497 0.243,5.256 C0.301,5.111 0.378,4.976 0.464,4.850 L9.374,12.427 L0.360,19.912 ZM12.833,15.369 L12.835,15.369 C12.855,15.385 12.877,15.396 12.902,15.404 C12.908,15.410 12.913,15.410 12.915,15.410 C12.939,15.421 12.963,15.427 12.987,15.427 L12.990,15.427 C13.011,15.427 13.039,15.421 13.062,15.411 C13.068,15.410 13.069,15.410 13.079,15.404 C13.101,15.399 13.122,15.385 13.141,15.369 L13.144,15.369 L16.239,12.794 L25.233,20.442 C25.191,20.479 25.146,20.520 25.101,20.551 C24.865,20.726 24.595,20.853 24.301,20.906 C24.180,20.927 24.058,20.939 23.935,20.939 L1.985,20.812 C1.519,20.808 1.092,20.638 0.756,20.358 C0.733,20.341 0.713,20.321 0.690,20.299 L9.766,12.758 L12.833,15.369 Z"/>
            <g filter="url(#icon-newsletter-filter)">
                <path fill-rule="evenodd"  class="icon__path--2" d="M17.454,16.993 L8.561,16.993 L11.448,11.993 L8.561,11.993 L13.007,4.291 L17.454,11.993 L14.567,11.993 L17.454,16.993 Z"/>
            </g>
        </svg>',
        'mod_programs' => '<svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" class="icon-programs" width="26px" height="22px">
            <defs>
                <filter filterUnits="userSpaceOnUse" id="icon-programs-filter" x="4px" y="0px" width="19px" height="22px">
                    <feOffset in="SourceAlpha" dx="0" dy="0" />
                    <feGaussianBlur result="blurOut" stdDeviation="2.236" />
                    <feFlood flood-color="rgb(42, 87, 17)" result="floodOut" />
                    <feComposite operator="atop" in="floodOut" in2="blurOut" />
                    <feComponentTransfer><feFuncA type="linear" slope="1"/></feComponentTransfer>
                    <feMerge>
                        <feMergeNode/>
                        <feMergeNode in="SourceGraphic"/>
                    </feMerge>
                </filter>
            </defs>
            <path fill-rule="evenodd" class="icon__path--3" d="M25.922,18.904 C25.919,19.338 25.787,19.732 25.569,20.056 L16.633,12.466 L25.627,4.998 C25.668,5.061 25.708,5.126 25.742,5.192 C25.769,5.242 25.790,5.286 25.813,5.333 C25.927,5.593 25.991,5.880 25.990,6.185 L25.922,18.904 ZM0.803,4.479 C1.148,4.180 1.593,4.005 2.075,4.005 L24.026,4.134 C24.133,4.134 24.234,4.144 24.334,4.161 C24.594,4.202 24.841,4.300 25.057,4.441 C25.139,4.495 25.219,4.553 25.293,4.620 L12.992,11.838 L0.803,4.479 ZM0.360,19.912 C0.191,19.656 0.075,19.349 0.036,19.020 C0.024,18.932 0.018,18.847 0.021,18.757 L0.089,6.037 C0.090,5.759 0.144,5.497 0.243,5.256 C0.301,5.111 0.378,4.976 0.464,4.850 L9.374,12.427 L0.360,19.912 ZM12.833,15.369 L12.835,15.369 C12.855,15.385 12.877,15.396 12.902,15.404 C12.908,15.410 12.913,15.410 12.915,15.410 C12.939,15.421 12.963,15.427 12.987,15.427 L12.990,15.427 C13.011,15.427 13.039,15.421 13.062,15.411 C13.068,15.410 13.069,15.410 13.079,15.404 C13.101,15.399 13.122,15.385 13.141,15.369 L13.144,15.369 L16.239,12.794 L25.233,20.442 C25.191,20.479 25.146,20.520 25.101,20.551 C24.865,20.726 24.595,20.853 24.301,20.906 C24.180,20.927 24.058,20.939 23.935,20.939 L1.985,20.812 C1.519,20.808 1.092,20.638 0.756,20.358 C0.733,20.341 0.713,20.321 0.690,20.299 L9.766,12.758 L12.833,15.369 Z"/>
            <g filter="url(#icon-programs-filter)">
                <path fill-rule="evenodd"  class="icon__path--2" d="M17.454,16.993 L8.561,16.993 L11.448,11.993 L8.561,11.993 L13.007,4.291 L17.454,11.993 L14.567,11.993 L17.454,16.993 Z"/>
            </g>
        </svg>',    
        'mod_gallery' => '<svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" class="icon-gallery" width="27px" height="28px">
            <defs>
                <filter filterUnits="userSpaceOnUse" id="icon-gallery-filter" x="0px" y="0px" width="27px" height="28px">
                    <feOffset in="SourceAlpha" dx="0" dy="0" />
                    <feGaussianBlur result="blurOut" stdDeviation="2.236" />
                    <feFlood flood-color="rgb(42, 87, 17)" result="floodOut" />
                    <feComposite operator="atop" in="floodOut" in2="blurOut" />
                    <feComponentTransfer><feFuncA type="linear" slope="1"/></feComponentTransfer>
                    <feMerge>
                        <feMergeNode/>
                        <feMergeNode in="SourceGraphic"/>
                    </feMerge>
                </filter>
            </defs>
            <path fill-rule="evenodd"  class="icon__path--3" d="M23.461,20.955 L3.411,20.955 C2.581,20.955 1.907,20.281 1.907,19.451 L1.907,7.423 C1.907,6.593 2.581,5.919 3.411,5.919 L23.461,5.919 C24.291,5.919 24.965,6.593 24.965,7.423 L24.965,19.451 C24.965,20.281 24.291,20.955 23.461,20.955 ZM24.000,8.000 L22.000,8.000 L22.000,9.981 L24.000,9.981 L24.000,8.000 Z"/>
            <g filter="url(#icon-gallery-filter)">
                <path fill-rule="evenodd"  class="icon__path--2" d="M12.543,4.950 C17.256,4.950 21.078,8.772 21.078,13.486 C21.078,18.200 17.256,22.022 12.543,22.022 C7.829,22.022 4.007,18.200 4.007,13.486 C4.007,8.772 7.829,4.950 12.543,4.950 Z"/>
            </g>
            <path fill-rule="evenodd" class="icon__path--4" d="M12.544,8.038 C15.552,8.038 17.991,10.477 17.991,13.485 C17.991,16.494 15.552,18.933 12.544,18.933 C9.536,18.933 7.097,16.494 7.097,13.485 C7.097,10.477 9.536,8.038 12.544,8.038 Z"/>
        </svg>',
        'mod_calendar' => '<svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" class="icon-calendar" width="24px" height="29px">
            <defs>
                <filter filterUnits="userSpaceOnUse" id="icon-calendar-filter" x="0px" y="0px" width="24px" height="14px">
                    <feOffset in="SourceAlpha" dx="0" dy="0" />
                    <feGaussianBlur result="blurOut" stdDeviation="2.236" />
                    <feFlood flood-color="rgb(42, 87, 17)" result="floodOut" />
                    <feComposite operator="atop" in="floodOut" in2="blurOut" />
                    <feComponentTransfer><feFuncA type="linear" slope="1"/></feComponentTransfer>
                    <feMerge>
                        <feMergeNode/>
                        <feMergeNode in="SourceGraphic"/>
                    </feMerge>
                </filter>
                <filter filterUnits="userSpaceOnUse" id="icon-calendar-filter-2" x="0px" y="6px" width="24px" height="23px">
                    <feOffset in="SourceAlpha" dx="0" dy="0" />
                    <feGaussianBlur result="blurOut" stdDeviation="2.236" />
                    <feFlood flood-color="rgb(42, 87, 17)" result="floodOut" />
                    <feComposite operator="atop" in="floodOut" in2="blurOut" />
                    <feComponentTransfer><feFuncA type="linear" slope="1"/></feComponentTransfer>
                    <feMerge>
                        <feMergeNode/>
                        <feMergeNode in="SourceGraphic"/>
                    </feMerge>
                </filter>
            </defs>
            <g filter="url(#icon-calendar-filter)">
            <path fill-rule="evenodd" class="icon__path--2" d="M17.495,8.964 C16.662,8.964 15.988,8.289 15.988,7.457 L15.988,5.538 C15.988,4.705 16.662,4.031 17.495,4.031 C18.327,4.031 19.002,4.705 19.002,5.538 L19.002,7.457 C19.002,8.289 18.327,8.964 17.495,8.964 ZM5.495,8.964 C4.662,8.964 3.988,8.289 3.988,7.457 L3.988,5.538 C3.988,4.705 4.662,4.031 5.495,4.031 C6.327,4.031 7.002,4.705 7.002,5.538 L7.002,7.457 C7.002,8.289 6.327,8.964 5.495,8.964 Z"/>
            </g>
            <path fill-rule="evenodd" class="icon__path--1" d="M21.000,28.000 L2.000,28.000 C0.895,28.000 -0.000,27.104 -0.000,26.000 L-0.000,8.000 C-0.000,6.895 0.895,6.000 2.000,6.000 L3.036,6.000 L3.036,8.019 C3.036,9.097 4.158,9.971 5.542,9.971 C6.926,9.971 8.048,9.097 8.048,8.019 L8.048,6.000 L14.889,6.000 L14.889,8.019 C14.889,9.097 16.034,9.971 17.445,9.971 C18.857,9.971 20.002,9.097 20.002,8.019 L20.002,6.000 L21.000,6.000 C22.104,6.000 23.000,6.895 23.000,8.000 L23.000,26.000 C23.000,27.104 22.104,28.000 21.000,28.000 Z"/>
            <g filter="url(#icon-calendar-filter-2)">
            <path fill-rule="evenodd" class="icon__path--2" d="M11.503,23.996 C7.886,23.996 4.959,21.058 4.959,17.452 C4.959,13.836 7.897,10.908 11.503,10.908 C15.108,10.908 18.047,13.836 18.047,17.452 C18.047,21.058 15.119,23.996 11.503,23.996 ZM11.503,13.012 C9.057,13.012 7.063,14.998 7.063,17.452 C7.063,19.899 9.049,21.893 11.503,21.893 C13.957,21.893 15.944,19.899 15.944,17.452 C15.944,14.998 13.950,13.012 11.503,13.012 ZM13.846,17.994 L11.465,17.994 C11.181,17.994 10.959,17.772 10.959,17.488 L10.959,14.045 C10.959,13.761 11.181,13.539 11.465,13.539 C11.749,13.539 11.971,13.761 11.971,14.045 L11.971,16.982 L13.846,16.982 C14.130,16.982 14.352,17.204 14.352,17.488 C14.352,17.772 14.130,17.994 13.846,17.994 Z"/>
            </g>
        </svg>',
        'mod_contact' => '<svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" class="icon-contact" width="28px" height="27px">
            <defs>
                <filter filterUnits="userSpaceOnUse" id="icon-contact-filter" x="7px" y="0px" width="21px" height="27px">
                    <feOffset in="SourceAlpha" dx="0" dy="0" />
                    <feGaussianBlur result="blurOut" stdDeviation="2.236" />
                    <feFlood flood-color="rgb(42, 87, 17)" result="floodOut" />
                    <feComposite operator="atop" in="floodOut" in2="blurOut" />
                    <feComponentTransfer><feFuncA type="linear" slope="1"/></feComponentTransfer>
                    <feMerge>
                        <feMergeNode/>
                        <feMergeNode in="SourceGraphic"/>
                </feMerge>
                </filter>
            </defs>
            <path fill-rule="evenodd"  class="icon__path--3" d="M25.922,16.904 C25.919,17.338 25.787,17.732 25.569,18.056 L16.633,10.466 L25.627,2.998 C25.668,3.061 25.708,3.126 25.742,3.192 C25.769,3.242 25.790,3.286 25.813,3.333 C25.926,3.593 25.991,3.880 25.990,4.185 L25.922,16.904 ZM0.803,2.479 C1.148,2.180 1.593,2.005 2.075,2.005 L24.026,2.134 C24.133,2.134 24.234,2.144 24.334,2.161 C24.594,2.202 24.841,2.300 25.057,2.441 C25.139,2.495 25.219,2.553 25.293,2.620 L12.992,9.838 L0.803,2.479 ZM0.360,17.912 C0.191,17.656 0.075,17.349 0.036,17.020 C0.024,16.932 0.018,16.847 0.021,16.757 L0.089,4.037 C0.090,3.759 0.144,3.497 0.243,3.256 C0.301,3.111 0.378,2.977 0.463,2.850 L9.374,10.427 L0.360,17.912 ZM12.833,13.369 L12.835,13.369 C12.855,13.385 12.877,13.396 12.902,13.404 C12.908,13.410 12.913,13.410 12.915,13.410 C12.939,13.421 12.963,13.427 12.987,13.427 L12.990,13.427 C13.011,13.427 13.039,13.421 13.062,13.411 C13.068,13.410 13.069,13.410 13.078,13.404 C13.101,13.399 13.122,13.385 13.141,13.369 L13.144,13.369 L16.239,10.795 L25.233,18.441 C25.191,18.479 25.146,18.520 25.101,18.551 C24.865,18.726 24.595,18.853 24.301,18.906 C24.180,18.927 24.058,18.939 23.935,18.939 L1.985,18.812 C1.519,18.808 1.092,18.639 0.756,18.358 C0.733,18.341 0.713,18.321 0.690,18.299 L9.766,10.758 L12.833,13.369 Z"/>
            <g filter="url(#icon-contact-filter)">
                <path fill-rule="evenodd"  class="icon__path--2" d="M22.728,6.729 L18.776,12.771 L16.185,11.074 L20.140,5.035 C20.606,4.322 21.566,4.124 22.279,4.588 C22.994,5.057 23.195,6.015 22.728,6.729 ZM13.673,20.143 L11.472,18.706 L15.991,11.798 L18.191,13.236 L13.673,20.143 ZM11.107,21.662 L11.234,19.399 L13.130,20.639 L11.107,21.662 Z"/>
            </g>
        </svg>',
        'mod_forum' => '<svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" class="icon-forum" width="37px" height="28px">
            <defs>
                <filter filterUnits="userSpaceOnUse" id="icon-forum-filter" x="4px" y="0px" width="33px" height="26px">
                    <feOffset in="SourceAlpha" dx="0" dy="0" />
                    <feGaussianBlur result="blurOut" stdDeviation="2.236" />
                    <feFlood flood-color="rgb(42, 87, 17)" result="floodOut" />
                    <feComposite operator="atop" in="floodOut" in2="blurOut" />
                    <feComponentTransfer><feFuncA type="linear" slope="1"/></feComponentTransfer>
                    <feMerge>
                        <feMergeNode/>
                        <feMergeNode in="SourceGraphic"/>
                    </feMerge>
                </filter>
            </defs>
            <path fill-rule="evenodd"  class="icon__path--1" d="M0.002,17.882 C0.002,14.083 5.374,11.003 12.000,11.003 C18.626,11.003 23.998,14.083 23.998,17.882 C23.998,21.133 20.063,23.855 14.776,24.574 C15.207,25.790 15.924,27.224 17.051,27.940 C14.880,27.943 12.740,26.246 11.272,24.747 C4.985,24.532 0.002,21.541 0.002,17.882 Z"/>
            <g filter="url(#icon-forum-filter)">
                <path fill-rule="evenodd"  class="icon__path--2" d="M31.998,10.882 C31.998,7.083 26.626,4.003 20.000,4.003 C13.374,4.003 8.002,7.083 8.002,10.882 C8.002,14.133 11.936,16.855 17.224,17.574 C16.793,18.790 16.075,20.224 14.949,20.940 C17.120,20.943 19.260,19.246 20.727,17.747 C27.015,17.532 31.998,14.541 31.998,10.882 Z"/>
            </g>
        </svg>',
        'mod_login' => '<svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" class="icon-login" width="30px" height="36px" viewBox="0 0 10 12">
            <defs>
                <filter filterUnits="userSpaceOnUse" id="icon-login-filter" x="0px" y="0px" width="10px" height="12px">
                    <feOffset in="SourceAlpha" dx="0" dy="1" />
                    <feGaussianBlur result="blurOut" stdDeviation="1" />
                    <feFlood flood-color="rgb(49, 101, 20)" result="floodOut" />
                    <feComposite operator="atop" in="floodOut" in2="blurOut" />
                    <feComponentTransfer><feFuncA type="linear" slope="1"/></feComponentTransfer>
                    <feMerge>
                        <feMergeNode/>
                        <feMergeNode in="SourceGraphic"/>
                    </feMerge>
                </filter>
            </defs>
            <g filter="url(#icon-login-filter)">
                <path fill-rule="evenodd" class="icon__path--2" d="M8.009,8.158 C8.009,8.159 8.009,8.159 8.009,8.159 L8.009,8.270 C8.009,8.660 7.714,8.974 7.350,8.974 L1.679,8.974 C1.316,8.974 1.021,8.660 1.021,8.270 L1.021,3.984 L2.008,3.984 L2.011,2.641 C2.023,1.198 3.158,0.038 4.543,0.053 C5.928,0.072 7.044,1.260 7.033,2.702 L7.029,3.984 L8.009,3.984 L8.009,4.141 L8.009,4.141 L8.009,8.158 ZM3.830,8.102 L4.520,8.102 L4.520,8.000 L5.185,8.000 L4.965,6.484 L4.934,6.288 C4.935,6.288 4.935,6.287 4.936,6.286 L4.934,6.275 C5.116,6.137 5.232,5.915 5.240,5.661 C5.223,5.273 4.910,4.961 4.520,4.959 L4.520,4.858 C4.518,4.858 4.517,4.857 4.515,4.857 C4.112,4.857 3.784,5.205 3.784,5.637 C3.784,5.909 3.916,6.148 4.114,6.287 L3.830,8.102 ZM4.536,0.931 C3.543,0.921 3.018,1.692 3.012,2.650 L3.008,3.984 L6.027,3.984 L6.031,2.695 C6.037,1.734 5.529,0.945 4.536,0.931 Z"/>
            </g>
        </svg>',
        'mod_timetable' => '<svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" class="icon-timetable" width="24px" height="31px">
            <defs>
                <filter filterUnits="userSpaceOnUse" id="icon-timetable-filter" x="4px" y="0px" width="17px" height="31px">
                    <feOffset in="SourceAlpha" dx="0" dy="0" />
                    <feGaussianBlur result="blurOut" stdDeviation="2.236" />
                    <feFlood flood-color="rgb(42, 87, 17)" result="floodOut" />
                    <feComposite operator="atop" in="floodOut" in2="blurOut" />
                    <feComponentTransfer><feFuncA type="linear" slope="1"/></feComponentTransfer>
                    <feMerge>
                        <feMergeNode/>
                        <feMergeNode in="SourceGraphic"/>
                    </feMerge>
                </filter>
            </defs>
            <path fill-rule="evenodd" class="icon__path--5" d="M22.000,29.000 L2.000,29.000 C0.895,29.000 -0.000,28.105 -0.000,27.000 L-0.000,12.000 C-0.000,10.895 0.895,10.000 2.000,10.000 L4.686,10.000 L12.000,2.686 L19.314,10.000 L22.000,10.000 C23.105,10.000 24.000,10.895 24.000,12.000 L24.000,27.000 C24.000,28.105 23.105,29.000 22.000,29.000 ZM13.000,26.000 L16.000,26.000 L16.000,23.000 L13.000,23.000 L13.000,26.000 ZM13.000,21.000 L16.000,21.000 L16.000,18.000 L13.000,18.000 L13.000,21.000 ZM13.000,16.000 L16.000,16.000 L16.000,13.000 L13.000,13.000 L13.000,16.000 ZM8.000,26.000 L11.000,26.000 L11.000,23.000 L8.000,23.000 L8.000,26.000 ZM8.000,21.000 L11.000,21.000 L11.000,18.000 L8.000,18.000 L8.000,21.000 ZM8.000,16.000 L11.000,16.000 L11.000,13.000 L8.000,13.000 L8.000,16.000 ZM6.000,13.000 L3.000,13.000 L3.000,16.000 L6.000,16.000 L6.000,13.000 ZM6.000,18.000 L3.000,18.000 L3.000,21.000 L6.000,21.000 L6.000,18.000 ZM6.000,23.000 L3.000,23.000 L3.000,26.000 L6.000,26.000 L6.000,23.000 ZM12.000,4.100 L6.100,10.000 L17.899,10.000 L12.000,4.100 ZM21.000,13.000 L18.000,13.000 L18.000,16.000 L21.000,16.000 L21.000,13.000 ZM21.000,18.000 L18.000,18.000 L18.000,21.000 L21.000,21.000 L21.000,18.000 ZM21.000,23.000 L18.000,23.000 L18.000,26.000 L21.000,26.000 L21.000,23.000 Z"/>
            <g filter="url(#icon-timetable-filter)">
                <path fill-rule="evenodd"  class="icon__path--2" d="M13.000,26.000 L13.000,23.000 L16.000,23.000 L16.000,26.000 L13.000,26.000 ZM12.000,6.062 C11.448,6.062 11.000,5.615 11.000,5.062 C11.000,4.510 11.448,4.062 12.000,4.062 C12.552,4.062 13.000,4.510 13.000,5.062 C13.000,5.615 12.552,6.062 12.000,6.062 ZM11.000,21.000 L8.000,21.000 L8.000,18.000 L11.000,18.000 L11.000,21.000 Z"/>
            </g>
        </svg>',
        'mod_video' => '<svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" class="icon-video" width="27px" height="24px">
            <defs>
                <filter filterUnits="userSpaceOnUse" id="icon-video-filter" x="5px" y="0px" width="22px" height="24px">
                    <feOffset in="SourceAlpha" dx="0" dy="0" />
                    <feGaussianBlur result="blurOut" stdDeviation="2.236" />
                    <feFlood flood-color="rgb(42, 87, 17)" result="floodOut" />
                    <feComposite operator="atop" in="floodOut" in2="blurOut" />
                    <feComponentTransfer><feFuncA type="linear" slope="1"/></feComponentTransfer>
                    <feMerge>
                        <feMergeNode/>
                        <feMergeNode in="SourceGraphic"/>
                    </feMerge>
                </filter>
            </defs>
            <path fill-rule="evenodd" class="icon__path--1" d="M25.000,23.000 L2.000,23.000 C0.895,23.000 -0.000,22.104 -0.000,21.000 L-0.000,3.000 C-0.000,1.895 0.895,1.000 2.000,1.000 L25.000,1.000 C26.104,1.000 27.000,1.895 27.000,3.000 L27.000,21.000 C27.000,22.104 26.104,23.000 25.000,23.000 ZM22.000,21.000 L25.000,21.000 L25.000,18.000 L22.000,18.000 L22.000,21.000 ZM17.000,21.000 L20.000,21.000 L20.000,18.000 L17.000,18.000 L17.000,21.000 ZM12.000,21.000 L15.000,21.000 L15.000,18.000 L12.000,18.000 L12.000,21.000 ZM7.000,21.000 L10.000,21.000 L10.000,18.000 L7.000,18.000 L7.000,21.000 ZM2.000,21.000 L5.000,21.000 L5.000,18.000 L2.000,18.000 L2.000,21.000 ZM5.000,3.000 L2.000,3.000 L2.000,6.000 L5.000,6.000 L5.000,3.000 ZM10.000,3.000 L7.000,3.000 L7.000,6.000 L10.000,6.000 L10.000,3.000 ZM15.000,3.000 L12.000,3.000 L12.000,6.000 L15.000,6.000 L15.000,3.000 ZM20.000,3.000 L17.000,3.000 L17.000,6.000 L20.000,6.000 L20.000,3.000 ZM25.000,3.000 L22.000,3.000 L22.000,6.000 L25.000,6.000 L25.000,3.000 ZM25.031,6.969 L2.000,6.969 L2.000,16.968 L25.031,16.968 L25.031,6.969 Z"/>
            <g filter="url(#icon-video-filter)">
                <path fill-rule="evenodd" class="icon__path--2" d="M21.164,11.962 L8.995,18.989 L8.995,4.936 L21.164,11.962 Z"/>
            </g>
        </svg>',
        'mod_jokes' => '<svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" class="icon-jokes" width="31px" height="37px">
            <defs>
                <filter filterUnits="userSpaceOnUse" id="icon-jokes-filter" x="3px" y="0px" width="28px" height="37px"  >
                    <feOffset in="SourceAlpha" dx="0" dy="0" />
                    <feGaussianBlur result="blurOut" stdDeviation="2.236" />
                    <feFlood flood-color="rgb(42, 87, 17)" result="floodOut" />
                    <feComposite operator="atop" in="floodOut" in2="blurOut" />
                    <feComponentTransfer><feFuncA type="linear" slope="1"/></feComponentTransfer>
                    <feMerge>
                        <feMergeNode/>
                        <feMergeNode in="SourceGraphic"/>
                    </feMerge>
                </filter>
            </defs>
            <path fill-rule="evenodd" class="icon__path--5" d="M18.448,15.896 C17.118,10.932 13.151,7.523 8.979,7.374 C8.206,7.347 7.426,7.431 6.655,7.637 C1.719,8.960 -1.005,14.802 0.572,20.686 C2.055,26.221 6.814,29.821 11.483,29.128 L11.404,30.526 C11.652,30.460 11.689,30.595 12.012,30.508 C12.150,30.471 12.222,30.425 12.286,30.378 C13.088,33.203 13.949,34.105 14.883,34.863 C15.456,35.329 16.093,34.996 15.389,34.316 C14.563,33.519 13.749,32.799 13.006,30.186 C13.083,30.195 13.168,30.198 13.304,30.162 C13.317,30.158 13.328,30.155 13.340,30.151 C13.353,30.148 13.365,30.146 13.378,30.142 C13.701,30.055 13.665,29.921 13.988,29.834 L13.220,28.663 C17.609,26.928 19.931,21.431 18.448,15.896 Z"/>
            <g filter="url(#icon-jokes-filter)">
                <path fill-rule="evenodd"  class="icon__path--2" d="M8.223,13.145 C9.498,8.387 13.300,5.121 17.298,4.978 C18.039,4.951 18.786,5.032 19.525,5.230 C24.256,6.497 26.866,12.096 25.355,17.735 C23.934,23.039 19.373,26.489 14.899,25.825 L14.975,27.165 C14.736,27.101 14.702,27.230 14.392,27.147 C14.259,27.112 14.190,27.068 14.129,27.022 C13.360,29.730 12.535,30.594 11.640,31.321 C11.091,31.767 10.481,31.448 11.156,30.797 C11.947,30.033 12.727,29.343 13.439,26.839 C13.365,26.848 13.284,26.850 13.154,26.816 C13.141,26.812 13.130,26.809 13.118,26.805 C13.107,26.802 13.095,26.800 13.083,26.796 C12.773,26.714 12.808,26.584 12.498,26.501 L13.234,25.379 C9.027,23.717 6.802,18.449 8.223,13.145 Z"/>
            </g>
        </svg>',
    );
?>
</head>
<body>
    <div class="page" tabindex="-1">
        <ul class="skip-links list-unstyled">
            <li><a href="#main-menu"><?php echo __('skip to main menu')?></a></li> 
            <li><a href="#add-menu"><?php echo __('skip to additional menu')?></a></li> 
            <li><a href="#link-content"><?php echo __('skip to content')?></a></li>
            <li><a href="#search"><?php echo __('skip to search')?></a></li>
            <li><a href="mapa_strony"><?php echo __('sitemap')?></a></li>
        </ul>