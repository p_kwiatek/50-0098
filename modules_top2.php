<div id="modules-top2" class="modules-top2 modules-content">
    <?php if ($numRowTop2Modules > 0): ?>
        <?php
            $href = '/^<a.*?href=(["\'])(.*?)\1.*$/';
            
            $links = array();
            
            for ($i = 0; $i < $numRowTop2Modules; $i++) {
                $tmp = get_module($outRowTop2Modules[$i]['mod_name']);
                
                preg_match($href, $tmp, $m);

                if ($m[2] != '') {
                    $links[] = $m[2];
                } else {
                    $links[] = trans_url_name($outRowTop2Modules[$i]['name']);
                }
            }
            
            $modules_color2 = array(
                'mod_forum',
            );
            
            $module_grid_classes = array(1 => "module-1", 2 => "module-2", 3 => "module-3");
            $module_grid_class = array_key_exists($numRowTop2Modules, $module_grid_classes) ? $module_grid_classes[$numRowTop2Modules] : $module_grid_classes[3];
        ?>
        
        <?php for ($i = 0; $i < $numRowTop2Modules; $i++): ?>
            <div class="module <?php echo $module_grid_class; ?> module-common <?php echo in_array($outRowTop2Modules[$i]['mod_name'], $modules_color2) ? 'color2' : ''; ?>" id="<?php echo $outRowTop2Modules[$i]['mod_name']; ?>">
                <div class="modules-content__holder">
                    <div class="modules-content__icon">
                        <?php if(array_key_exists($outRowTop2Modules[$i]['mod_name'], $icons)): echo $icons[$outRowTop2Modules[$i]['mod_name']]; endif; ?>
                    </div>
                    <h2 class="modules-content__name"><?php echo $outRowTop2Modules[$i]['name'] ?></h2>
                    <div class="modules-content__content"><?php echo get_module($outRowTop2Modules[$i]['mod_name']); ?></div>
                </div>
            </div>
        <?php endfor; ?>
    <?php endif; ?>
</div>