<a id="top"></a>
<div class="container toolbar__holder">
    <div class="row">
        <div class="col-xs-12">
            <nav id="toolbar" class="toolbar">
                <?php
                    $pageInfo['name'] = str_replace('w ', 'w&nbsp;', $pageInfo['name']);
                    $pageInfo['name'] = str_replace('im ', 'im&nbsp;', $pageInfo['name']);
                ?>
                <h1 class="sr-only"><?php echo $pageInfo['name']; ?></h1>
                <ul class="toolbar__links toolbar__links--left">
                    <li>
                        <a href="index.php" class="toolbar__home">
                            <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="17px" height="15px">
                                <defs>
                                    <filter filterUnits="userSpaceOnUse" id="home-filter" x="0px" y="0px" width="17px" height="15px"  >
                                        <feOffset in="SourceAlpha" dx="0" dy="1" />
                                        <feGaussianBlur result="blurOut" stdDeviation="1" />
                                        <feFlood flood-color="rgb(0, 0, 0)" result="floodOut" />
                                        <feComposite operator="atop" in="floodOut" in2="blurOut" />
                                        <feComponentTransfer><feFuncA type="linear" slope="0.2"/></feComponentTransfer>
                                        <feMerge>
                                            <feMergeNode/>
                                            <feMergeNode in="SourceGraphic"/>
                                        </feMerge>
                                    </filter>
                                </defs>
                                <g filter="url(#home-filter)">
                                    <path fill-rule="evenodd" d="M4.014,6.962 L4.014,11.975 L7.013,11.975 L7.013,8.414 C7.013,8.164 7.221,7.961 7.479,7.961 L9.546,7.961 C9.803,7.961 10.012,8.164 10.012,8.414 L10.012,11.975 L12.984,11.975 L13.010,6.962 L14.994,6.962 L8.511,0.939 L1.968,6.962 L4.014,6.962 Z"/>
                                </g>
                            </svg>
                            <span><?php echo __('home page'); ?></span>
                        </a>
                    </li>
                    <li>
                        <a href="mapa_strony" class="toolbar__sitemap">
                            <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="13px" height="12px">
                                <defs>
                                    <filter filterUnits="userSpaceOnUse" id="sitemap-filter" x="0px" y="0px" width="13px" height="12px"  >
                                        <feOffset in="SourceAlpha" dx="0" dy="1" />
                                        <feGaussianBlur result="blurOut" stdDeviation="1" />
                                        <feFlood flood-color="rgb(0, 0, 0)" result="floodOut" />
                                        <feComposite operator="atop" in="floodOut" in2="blurOut" />
                                        <feComponentTransfer><feFuncA type="linear" slope="0.2"/></feComponentTransfer>
                                        <feMerge>
                                            <feMergeNode/>
                                            <feMergeNode in="SourceGraphic"/>
                                        </feMerge>
                                    </filter>
                                </defs>
                                <g filter="url(#sitemap-filter)">
                                    <path fill-rule="evenodd" d="M11.000,9.000 L7.000,9.000 L7.000,5.000 L8.015,5.000 L8.015,4.015 L9.015,4.015 L9.015,5.000 L11.000,5.000 L11.000,9.000 ZM4.000,-0.000 L8.000,-0.000 L8.000,4.000 L4.000,4.000 L4.000,-0.000 ZM4.015,4.015 L4.015,5.000 L5.000,5.000 L5.000,9.000 L1.000,9.000 L1.000,5.000 L3.015,5.000 L3.015,4.015 L4.015,4.015 Z"/>
                                </g>
                            </svg>
                            <span><?php echo __('sitemap') ?></span>
                        </a>
                    </li>
                    <li>
                        <a href="<?php echo $pageInfo['bip'] ?>" class="toolbar__bip">
                            <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="48px" height="22px">
                                <defs>
                                    <filter filterUnits="userSpaceOnUse" id="bip-filter" x="0px" y="0px" width="48px" height="22px">
                                        <feOffset in="SourceAlpha" dx="0" dy="1" />
                                        <feGaussianBlur result="blurOut" stdDeviation="1" />
                                        <feFlood flood-color="rgb(0, 0, 0)" result="floodOut" />
                                        <feComposite operator="atop" in="floodOut" in2="blurOut" />
                                        <feComponentTransfer><feFuncA type="linear" slope="0.2"/></feComponentTransfer>
                                        <feMerge>
                                            <feMergeNode/>
                                            <feMergeNode in="SourceGraphic"/>
                                        </feMerge>
                                    </filter>
                                </defs>
                                <g filter="url(#bip-filter)">
                                    <path fill-rule="evenodd" d="M40.174,15.395 C39.114,15.395 38.132,15.084 37.283,14.573 L37.283,17.634 C37.283,18.398 36.666,19.014 35.904,19.014 C35.142,19.014 34.524,18.398 34.524,17.634 L34.524,9.743 C34.524,6.628 37.058,4.094 40.174,4.094 C43.291,4.094 45.825,6.628 45.825,9.743 C45.825,12.860 43.291,15.395 40.174,15.395 ZM40.174,6.855 C38.579,6.855 37.283,8.150 37.283,9.743 C37.283,11.338 38.579,12.634 40.174,12.634 C41.767,12.634 43.065,11.338 43.065,9.743 C43.065,8.150 41.767,6.855 40.174,6.855 ZM31.056,15.249 C29.024,14.223 28.789,11.881 28.789,10.210 L28.789,5.491 C28.789,4.730 29.406,4.111 30.169,4.111 C30.930,4.111 31.547,4.730 31.547,5.491 L31.547,10.210 C31.547,12.403 32.111,12.690 32.297,12.782 C32.977,13.124 33.251,13.954 32.910,14.634 C32.568,15.315 31.737,15.589 31.056,15.249 ZM30.169,3.184 C29.330,3.184 28.651,2.506 28.651,1.668 C28.651,0.828 29.330,0.147 30.169,0.147 C31.006,0.147 31.686,0.828 31.686,1.668 C31.686,2.506 31.006,3.184 30.169,3.184 ZM21.679,15.402 C18.566,15.402 16.036,12.871 16.030,9.761 C16.030,9.757 16.028,9.756 16.028,9.752 L16.028,1.523 C16.028,0.761 16.645,0.143 17.407,0.143 C18.171,0.143 18.786,0.761 18.786,1.523 L18.786,4.923 C19.636,4.412 20.617,4.101 21.679,4.101 C24.795,4.101 27.330,6.635 27.330,9.752 C27.330,12.866 24.795,15.402 21.679,15.402 ZM21.679,6.861 C20.085,6.861 18.786,8.158 18.786,9.752 C18.786,11.347 20.085,12.642 21.679,12.642 C23.272,12.642 24.569,11.347 24.569,9.752 C24.569,8.158 23.272,6.861 21.679,6.861 ZM12.001,4.101 L12.001,14.538 L1.720,4.101 L12.001,4.101 Z"/>
                                </g>
                            </svg>
                        </a>
                    </li>
                </ul>
                <ul class="toolbar__links toolbar__links--right">
                    <li class="toolbar__fonts">
                        <span class="sr-only"><?php echo __('font size'); ?></span>
                        <ul>
                            <li>
                                <a href="ch_style.php?style=0">
                                    <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="14px" height="16px">
                                        <defs>
                                            <filter filterUnits="userSpaceOnUse" id="font-default-filter" x="0px" y="0px" width="14px" height="16px">
                                                <feOffset in="SourceAlpha" dx="0" dy="1" />
                                                <feGaussianBlur result="blurOut" stdDeviation="1" />
                                                <feFlood flood-color="rgb(0, 0, 0)" result="floodOut" />
                                                <feComposite operator="atop" in="floodOut" in2="blurOut" />
                                                <feComponentTransfer><feFuncA type="linear" slope="0.2"/></feComponentTransfer>
                                                <feMerge>
                                                    <feMergeNode/>
                                                    <feMergeNode in="SourceGraphic"/>
                                                </feMerge>
                                            </filter>
                                        </defs>
                                        <g filter="url(#font-default-filter)">
                                            <path fill-rule="evenodd" d="M3.358,13.000 L4.024,10.354 L8.758,10.354 L9.424,13.000 L11.440,13.000 L8.380,0.670 L4.402,0.670 L1.360,13.000 L3.358,13.000 ZM6.850,2.344 L8.362,8.590 L4.420,8.590 L5.950,2.344 L6.850,2.344 Z"/>
                                        </g>
                                    </svg>
                                    <span class="sr-only"><?php echo __('font default')?></span>
                                </a>
                            </li>
                            <li>
                                <a href="ch_style.php?style=r1">
                                    <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="21px" height="21px">
                                        <defs>
                                            <filter filterUnits="userSpaceOnUse" id="font-bigger-filter" x="0px" y="0px" width="21px" height="21px">
                                                <feOffset in="SourceAlpha" dx="0" dy="1" />
                                                <feGaussianBlur result="blurOut" stdDeviation="1" />
                                                <feFlood flood-color="rgb(0, 0, 0)" result="floodOut" />
                                                <feComposite operator="atop" in="floodOut" in2="blurOut" />
                                                <feComponentTransfer><feFuncA type="linear" slope="0.2"/></feComponentTransfer>
                                                <feMerge>
                                                    <feMergeNode/>
                                                    <feMergeNode in="SourceGraphic"/>
                                                </feMerge>
                                            </filter>
                                        </defs>
                                        <g filter="url(#font-bigger-filter)">
                                            <path fill-rule="evenodd" d="M17.000,4.000 L17.000,6.000 L15.000,6.000 L15.000,4.000 L13.000,4.000 L13.000,2.000 L15.000,2.000 L15.000,-0.000 L17.000,-0.000 L17.000,2.000 L19.000,2.000 L19.000,4.000 L17.000,4.000 ZM11.296,18.000 L10.482,14.766 L4.696,14.766 L3.882,18.000 L1.440,18.000 L5.158,2.930 L10.020,2.930 L13.760,18.000 L11.296,18.000 ZM8.150,4.976 L7.050,4.976 L5.180,12.610 L9.998,12.610 L8.150,4.976 Z"/>
                                        </g>
                                    </svg>
                                    <span class="sr-only"><?php echo __('font bigger')?></span>
                                </a>
                            </li>
                            <li>
                                <a href="ch_style.php?style=r2">
                                    <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="30px" height="22px">
                                        <defs>
                                            <filter filterUnits="userSpaceOnUse" id="foter-biggest-filter" x="0px" y="0px" width="30px" height="22px"  >
                                                <feOffset in="SourceAlpha" dx="0" dy="1" />
                                                <feGaussianBlur result="blurOut" stdDeviation="1" />
                                                <feFlood flood-color="rgb(0, 0, 0)" result="floodOut" />
                                                <feComposite operator="atop" in="floodOut" in2="blurOut" />
                                                <feComponentTransfer><feFuncA type="linear" slope="0.2"/></feComponentTransfer>
                                                <feMerge>
                                                    <feMergeNode/>
                                                    <feMergeNode in="SourceGraphic"/>
                                                </feMerge>
                                            </filter>
                                        </defs>
                                        <g filter="url(#foter-biggest-filter)">
                                            <path fill-rule="evenodd" d="M26.000,4.000 L26.000,6.000 L24.000,6.000 L24.000,4.000 L22.000,4.000 L22.000,2.000 L24.000,2.000 L24.000,-0.000 L26.000,-0.000 L26.000,2.000 L28.000,2.000 L28.000,4.000 L26.000,4.000 ZM19.000,6.000 L17.000,6.000 L17.000,4.000 L15.000,4.000 L15.000,2.000 L17.000,2.000 L17.000,-0.000 L19.000,-0.000 L19.000,2.000 L21.000,2.000 L21.000,4.000 L19.000,4.000 L19.000,6.000 ZM13.168,19.000 L12.206,15.178 L5.368,15.178 L4.406,19.000 L1.520,19.000 L5.914,1.190 L11.660,1.190 L16.080,19.000 L13.168,19.000 ZM9.450,3.608 L8.150,3.608 L5.940,12.630 L11.634,12.630 L9.450,3.608 Z"/>
                                        </g>
                                    </svg>
                                    <span class="sr-only"><?php echo __('font biggest')?></span>
                                </a>
                            </li>
                        </ul>
                    </li>
                    <?php
                        if ($_SESSION['contr'] == 0) {
                            $set_contrast = 1;
                            $contrast_txt = __('contrast version');
                        } else {
                            $set_contrast = 0;
                            $contrast_txt = __('graphic version');
                        }
                    ?>
                    <li class="contrast">
                        <a href="ch_style.php?contr=<?php echo $set_contrast ?>" class="toolbar__contrast">
                            <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="23px" height="24px">
                                <defs>
                                    <filter filterUnits="userSpaceOnUse" id="contrast-filter" x="0px" y="0px" width="23px" height="24px"  >
                                        <feOffset in="SourceAlpha" dx="0" dy="1" />
                                        <feGaussianBlur result="blurOut" stdDeviation="1" />
                                        <feFlood flood-color="rgb(0, 0, 0)" result="floodOut" />
                                        <feComposite operator="atop" in="floodOut" in2="blurOut" />
                                        <feComponentTransfer><feFuncA type="linear" slope="0.2"/></feComponentTransfer>
                                        <feMerge>
                                            <feMergeNode/>
                                            <feMergeNode in="SourceGraphic"/>
                                        </feMerge>
                                    </filter>
                                </defs>
                                <g filter="url(#contrast-filter)">
                                    <path fill-rule="evenodd" d="M11.001,20.022 C5.476,20.022 0.997,15.543 0.997,10.018 C0.997,4.493 5.476,0.015 11.001,0.015 C16.526,0.015 21.005,4.493 21.005,10.018 C21.005,15.543 16.526,20.022 11.001,20.022 ZM11.001,1.987 C6.566,1.987 2.971,5.582 2.971,10.017 C2.971,14.452 6.566,18.048 11.001,18.048 C11.003,18.048 11.006,18.047 11.009,18.047 L11.009,1.988 C11.006,1.988 11.003,1.987 11.001,1.987 Z"/>
                                </g>
                            </svg>
                            <span class="sr-only"><?php echo __('font contrast'); ?></span>
                        </a>
                    </li>
                    <li id="searchbar" class="toolbar__search">
                        <a id="search" tabindex="-1" style="display: inline;"></a>
                        <h2 class="sr-only"><?php echo __('search')?></h2>
                        <form id="searchForm" name="f_szukaj" method="get" action="index.php">
                            <input name="c" type="hidden" value="search" />
                            <label for="kword"><span class="sr-only"><?php echo __('search query')?></span></label>
                            <input type="text" id="kword" name="kword" value="<?php echo __('search query'); ?>" onfocus="if (this.value=='<?php echo __('search query'); ?>') {this.value=''};" onblur="if (this.value=='') {this.value='<?php echo __('search query'); ?>'};"/>
                            <button type="submit" name="search" id="toolbar-search">
                                <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="18px" height="19px">
                                    <defs>
                                        <filter filterUnits="userSpaceOnUse" id="search-filter" x="0px" y="0px" width="18px" height="19px"  >
                                            <feOffset in="SourceAlpha" dx="0" dy="1" />
                                            <feGaussianBlur result="blurOut" stdDeviation="0" />
                                            <feFlood flood-color="rgb(255, 255, 255)" result="floodOut" />
                                            <feComposite operator="atop" in="floodOut" in2="blurOut" />
                                            <feComponentTransfer><feFuncA type="linear" slope="0.59"/></feComponentTransfer>
                                            <feMerge>
                                                <feMergeNode/>
                                                <feMergeNode in="SourceGraphic"/>
                                            </feMerge>
                                        </filter>
                                    </defs>
                                    <g filter="url(#search-filter)">
                                        <path fill-rule="evenodd" d="M10.975,13.996 C7.118,13.996 3.992,10.868 3.992,7.010 C3.992,3.151 7.118,0.023 10.975,0.023 C14.832,0.023 17.958,3.151 17.958,7.010 C17.958,10.868 14.832,13.996 10.975,13.996 ZM10.978,1.994 C8.208,1.994 5.963,4.240 5.963,7.011 C5.963,9.782 8.208,12.028 10.978,12.028 C13.747,12.028 15.993,9.782 15.993,7.011 C15.993,4.240 13.747,1.994 10.978,1.994 ZM6.625,14.049 L3.844,17.509 C3.198,18.169 2.160,18.147 1.492,17.525 L1.463,17.496 L1.435,17.467 C0.826,16.783 0.846,15.723 1.492,15.062 L4.605,12.101 C5.189,12.992 5.613,13.391 6.625,14.049 Z"/>
                                    </g>
                                </svg>
                                <span class="sr-only"><?php echo __('search action'); ?></span>    
                            </button>
                        </form>
                    </li>
                </ul>
            </nav>
        </div>
    </div>
</div>