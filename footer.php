<footer class="footer">
	<?php echo __('designed') ?>: <a href="http://szkolnastrona.pl/">Szkolnastrona.pl</a>
</footer>
<footer class="footer--mobile">
    <a href="mapa_strony"><?php echo __('sitemap') ?></a>
    <a href="#top" class="footer__gotop"><?php echo __('go to top'); ?></a>
</footer>
<?php
/*
 * Pobranie z zewnątrz
 */
echo get_url_content($external_text['wwwStart'], 'wwwStart', true);
?>
</div>
</body>
</html>