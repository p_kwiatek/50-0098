<?php
if ($showAll)
{
    ?>
<div class="gallery-wrapper">
    <h2 class="main-header"><span><?php echo $pageName?></span></h2>
    <ul class="gallery">
    <?php
    if (count($albums) > 0)
    {
        $n = 0;
        foreach ($albums as $value)
        {
            $n++;
            ?>
            <li>
                <a href="<?php echo $value['link']?>" class="photo">
                    <span class="sr-only"><?php echo __('enlarge image')?>: <?php echo $value['name']?></span>
                    <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" viewBox="0 0 369 239">
                        <defs>
                            <pattern id="<?php echo 'article-image-' . $n; ?>" patternUnits="objectBoundingBox" width="100%" height="100%">
                                <image height="100%" width="100%" preserveAspectRatio="xMidYMid slice" xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="files/<?php echo $lang?>/mini/<?php echo $value['file']; ?>"></image>
                            </pattern>
                        </defs>
                        <path fill-rule="evenodd" stroke-width="5px" fill="<?php echo 'url(#article-image-' . $n . ')'; ?>" d="M28.534,234.500 C28.534,234.485 28.537,234.470 28.537,234.455 C28.537,221.171 17.779,210.401 4.500,210.377 L4.500,4.500 L340.377,4.500 C340.401,17.779 351.171,28.536 364.455,28.536 C364.470,28.536 364.485,28.534 364.500,28.534 L364.500,234.500 L28.534,234.500 Z"/>
                    </svg>
                    <p class="photo-name"><?php echo $value['name']?></p> 
                </a>
            </li>
            <?php
        }	
    } else
    {
        ?>
        <p><?php echo __('no photo album added')?></p>
        <?php
    }
    ?>
</div>
    <?php
}
if ($showOne)
{
    ?>
<div class="gallery-wrapper">
    <h2 class="main-header"><span><?php echo $pageName?></span></h2>
        <?php 
        echo $message;
        ?>
	<?php 
	if ($showGallery)
	{
	    if (count($outRows) > 0)
	    {
                ?>
                <ul class="gallery">
                <?php
		$n = 0;
		foreach ($outRows as $value)
		{
		    $n++;
		    ?>
		    <li>
    			<a href="files/<?php echo $lang?>/<?php echo $value['file']?>" title="<?php echo __('enlarge image') . ': ' . $value['name']?>" data-fancybox-group="gallery" class="photo fancybox">
                    <span class="sr-only"><?php echo __('enlarge image')?>: <?php echo $value['name']?></span>
                    <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" viewBox="0 0 369 239">
                        <defs>
                            <pattern id="<?php echo 'article-image-' . $n; ?>" patternUnits="objectBoundingBox" width="100%" height="100%">
                                <image height="100%" width="100%" preserveAspectRatio="xMidYMid slice" xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="files/<?php echo $lang?>/mini/<?php echo $value['file']; ?>"></image>
                            </pattern>
                        </defs>
                        <path fill-rule="evenodd" stroke-width="5px" fill="<?php echo 'url(#article-image-' . $n . ')'; ?>" d="M28.534,234.500 C28.534,234.485 28.537,234.470 28.537,234.455 C28.537,221.171 17.779,210.401 4.500,210.377 L4.500,4.500 L340.377,4.500 C340.401,17.779 351.171,28.536 364.455,28.536 C364.470,28.536 364.485,28.534 364.500,28.534 L364.500,234.500 L28.534,234.500 Z"/>
                    </svg>
        			<?php
        			if (! check_html_text($value['name'], '') )
        			{
        			    ?>
        			    <p class="photo-name" aria-hidden="true"><?php echo $value['name']?></p>
        			    <?php
        			}
        		    ?>
                </a>
		    </li>
                <?php
		}
                ?>
                </ul>
                <?php
	    }
        }
    ?>
</div>
<?php
if ($showLoginForm)
{
    ?>
    <div class="main-text">
    <?php
    include( CMS_TEMPL . DS . 'form_login.php');
    ?>
    </div>
    <?php
}
?>
    
<?php
}
?>
