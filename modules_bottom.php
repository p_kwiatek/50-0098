<div id="modules-bottom" class="modules-bottom modules-content">
    <?php if ($numRowBottomModules > 0): ?>
        <?php
            $href = '/^<a.*?href=(["\'])(.*?)\1.*$/';
            
            $links = array();
            
            for ($i = 0; $i < $numRowBottomModules; $i++) {
                $tmp = get_module($outRowBottomModules[$i]['mod_name']);
                
                preg_match($href, $tmp, $m);

                if ($m[2] != '') {
                    $links[] = $m[2];
                } else {
                    $links[] = trans_url_name($outRowBottomModules[$i]['name']);
                }
            }
            
            $modules_color2 = array(
                'mod_forum',
            );
            
            $module_grid_classes = array(1 => "module-1", 2 => "module-2", 3 => "module-3");
            $module_grid_class = array_key_exists($numRowBottomModules, $module_grid_classes) ? $module_grid_classes[$numRowBottomModules] : $module_grid_classes[3];
        ?>
        
        <?php for ($i = 0; $i < $numRowBottomModules; $i++): ?>
            <div class="module <?php echo $module_grid_class; ?> module-common <?php echo in_array($outRowBottomModules[$i]['mod_name'], $modules_color2) ? 'color2' : ''; ?>" id="<?php echo $outRowBottomModules[$i]['mod_name']; ?>">
                <div class="modules-content__holder">
                    <div class="modules-content__icon">
                        <?php if(array_key_exists($outRowBottomModules[$i]['mod_name'], $icons)): echo $icons[$outRowBottomModules[$i]['mod_name']]; endif; ?>
                    </div>
                    <h2 class="modules-content__name"><?php echo $outRowBottomModules[$i]['name'] ?></h2>
                    <div class="modules-content__content"><?php echo get_module($outRowBottomModules[$i]['mod_name']); ?></div>
                </div>
            </div>
        <?php endfor; ?>
    <?php endif; ?>
</div>