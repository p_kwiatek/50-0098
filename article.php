<h2 class="main-header"><span><?php echo $pageName?></span></h2>
<div class="main-text">
<?php
echo $message;

if ($showLoginForm)
{
    include( CMS_TEMPL . DS . 'form_login.php');
}
		
if ($showArticle)
{
    echo '<div class="leadArticle">' . $article['lead_text'] . '</div>';
    
    echo $article['text'];
		
    if (! check_html_text($article['author'], '') )
    {
	?>
	<p class="author-name"><?php echo __('author')?>: <?php echo $article['author']?></p>
	<?php
    }
    
    ?>
</div>
<div>
    <?php
    /*
     *  Wypisanie plikow do pobrania
     */
    if ($numFiles > 0)
    {
	?>
	<div class="files-wrapper">
            
                <h3 class="files-header"><span><?php echo __('files')?></span></h3>
                <ul class="list-unstyled">
                <?php
                foreach ($outRowFiles as $row)
                {
                    $target = 'target="_blank" ';

                    if (filesize('download/'.$row['file']) > 5000000)
                    {
                        $url = 'download/'.$row['file'];
                    } else
                    {
                        $url = 'index.php?c=getfile&amp;id='.$row['id_file'];
                    }
                    if (trim($row['name']) == '')
                    {
                        $name = $row['file'];
                    } else
                    {
                        $name = $row['name'];
                    }			
                    $size = file_size('download/'.$row['file']);	
                    ?>
                    <li>
                        <a href="<?php echo $url?>" <?php echo $target?>>
                            <i class="icon-doc-text-inv icon" aria-hidden="true"></i>
                            <span class="title">
                                <?php echo $name?>
                                <span class="size">(<?php echo $size?>)</span>
                            </span>
                        </a>
                    </li>
                    <?php
                }
                ?>
                </ul>
            
	</div>
    <?php
    }
		
    /*
     *  Wypisanie zdjec
     */
    if ($numPhotos > 0)
    {	
	$i = 0;
	?>
	<div class="gallery-wrapper">
        <h3 class="gallery-header"><span><?php echo __('gallery')?></span></h3>
        <ul class="gallery">
        <?php
        foreach ($outRowPhotos as $row)
        {
            $i++;
            $noMargin = '';
            if ($i == $pageConfig['zawijaj'])
            {
                $noMargin = ' noMargin';
            }
            ?>
            <li>
                <a href="files/<?php echo $lang?>/<?php echo $row['file']?>" title="<?php echo __('enlarge image') . ': ' . $row['name']?>" data-fancybox-group="gallery" class="photo fancybox">
                    <span class="sr-only"><?php echo __('enlarge image')?>: <?php echo $row['name']?></span>
                    <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" viewBox="0 0 369 239">
                        <defs>
                            <pattern id="<?php echo 'article-image-' . $i; ?>" patternUnits="objectBoundingBox" width="100%" height="100%">
                                <image height="100%" width="100%" preserveAspectRatio="xMidYMid slice" xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="files/<?php echo $lang?>/mini/<?php echo $row['file']; ?>"></image>
                            </pattern>
                        </defs>
                        <path fill-rule="evenodd" stroke-width="5px" fill="<?php echo 'url(#article-image-' . $i . ')'; ?>" d="M28.534,234.500 C28.534,234.485 28.537,234.470 28.537,234.455 C28.537,221.171 17.779,210.401 4.500,210.377 L4.500,4.500 L340.377,4.500 C340.401,17.779 351.171,28.536 364.455,28.536 C364.470,28.536 364.485,28.534 364.500,28.534 L364.500,234.500 L28.534,234.500 Z"/>
                    </svg>
                    <?php
                    if (! check_html_text($row['name'], '') )
                    {
                        ?>
                        <p class="photo-name" aria-hidden="true"><?php echo $row['name']?></p>
                        <?php
                    }
                    ?>
                </a>
            </li>
        <?php
        }
        ?>
        </ul>
    </div>
    <?php
    }		
    if ($outSettings['pluginTweet'] == 'włącz')
    {
	 echo '<div class="Tweet"><iframe frameborder="0" scrolling="no" src="//platform.twitter.com/widgets/tweet_button.html" style="width:80px; height:30px;"></iframe></div>';  
    }

    if ($outSettings['pluginFB'] == 'włącz')
    {
        $color = 'light';
        if ($_SESSION['contr'] == 1)
        {
            $color = 'dark';
        }        
	$fb_url = urlencode('http://'.$pageInfo['host'].'/index.php?c=article&amp&id='. $_GET['id']);
	echo '<div class="FBLike"><iframe src=\'http://www.facebook.com/plugins/like.php?href='.$fb_url.'&amp;layout=standard&amp;show_faces=true&amp;width=400&amp;action=like&amp;font=tahoma&amp;colorscheme='.$color.'&amp;height=32&amp;show_faces=false\' scrolling="no" frameborder="0" style="border:none; overflow:hidden; width:400px; height:32px;"></iframe></div>';   
    }
}
?>
</div>